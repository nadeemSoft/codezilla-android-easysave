package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.models.BookedTicketModel;

import java.util.ArrayList;

public class MyBookedTicketAdapter extends RecyclerView.Adapter<MyBookedTicketAdapter.Booked> {
    ArrayList<BookedTicketModel> bookMyTicketList;
    Context context;


    public MyBookedTicketAdapter(Context context, ArrayList<BookedTicketModel> bookMyTicketList){
        this.context = context;
        this.bookMyTicketList = bookMyTicketList;
    }
    @Override
    public Booked onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_booked_ticket, parent, false);
        return new Booked(itemView);
    }

    @Override
    public void onBindViewHolder(Booked holder, int position) {
        BookedTicketModel model = bookMyTicketList.get(position);
        Picasso.with(context)
                .load(model.getImageUrl())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.ivImage);
        holder.tvName.setText(model.getName());
        holder.tvTye.setText(model.getCategory());
        holder.tvDate.setText(model.getBookedDate());
        holder.tvTime.setText(model.getBookedTimeSlot());
        holder.tvNoOfTicket.setText("Booked Ticket"+" "+model.getNoOfTickets());

    }

    @Override
    public int getItemCount() {
        return bookMyTicketList.size();
    }

    public class Booked extends RecyclerView.ViewHolder {
        TextView tvName, tvTye, tvDate,tvTime,tvNoOfTicket;
        ImageView ivImage;
        ConstraintLayout constraintLayout;
        public Booked(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvTye = (TextView) itemView.findViewById(R.id.tv_type);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            ivImage = (ImageView) itemView.findViewById(R.id.imageView);
            tvNoOfTicket = (TextView)itemView.findViewById(R.id.tv_noOfTicket);
            constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.constraint);
        }
    }
}
