package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by codezilla-10 on 24/2/18.
 */

public class ExpandablelistNavigationAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    public ExpandablelistNavigationAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listDataChild) {

        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listDataChild;
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        try {
            return this.listDataChild.get(this.listDataHeader.get(groupPosition)).size();

        } catch (NullPointerException e) {
            Log.e("Array", "is null");
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        try {

            return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                    .get(childPosition);

        } catch (NullPointerException e) {
            Log.e("Array", "is null");
        }

        return false;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {

        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandable_grouplist, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        ImageView lblListImg = (ImageView) convertView.findViewById(R.id.lblListImage);

        // lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        // lblListImg.setImageResource(R.drawable.profile);

        if (headerTitle == "Your History") {
            lblListImg.setImageResource(R.drawable.history);

        } else if (headerTitle == "Buy a Ticket") {
            lblListImg.setImageResource(R.drawable.bticket);
            //lblListImg. setImageDrawable(context.getResources().getDrawable(R.drawable.bticket));


        } else if (headerTitle == "Our Partners") {
            lblListImg.setImageResource(R.drawable.ourpartners);

        } /*else if (headerTitle == "About EasySave") {
            lblListImg.setImageResource(R.drawable.aboutus);

        }*/ else if (headerTitle == "Contact Us") {
            lblListImg.setImageResource(R.drawable.contactus);

        } else if (headerTitle == "Share With Friends") {
            lblListImg.setImageResource(R.drawable.share);

        } else if (headerTitle == "Weeklys") {
            lblListImg.setImageResource(R.drawable.weeklylarge);

        }  else if (headerTitle == "Like Us-Facebook") {

            /*lblListImg.setMaxHeight(32);
            lblListImg.setMaxWidth(32);*/

           // lblListHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f);
            lblListImg.setImageResource(R.drawable.facebooklarg);

        } else if (headerTitle == "Follow us on Instagram") {

            /*lblListImg.setMaxHeight(32);
            lblListImg.setMaxWidth(32);*/

            //lblListHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f);
            lblListImg.setImageResource(R.drawable.instagramlarg);

        }else if (headerTitle == "Logout") {
            lblListImg.setImageResource(R.drawable.logout);

        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandable_rowlist2, null);

        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        ImageView txtListChildImg = convertView.findViewById(R.id.lblListItemImage);

        txtListChild.setText(childText);
        txtListChildImg.setImageResource(R.drawable.dots);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
