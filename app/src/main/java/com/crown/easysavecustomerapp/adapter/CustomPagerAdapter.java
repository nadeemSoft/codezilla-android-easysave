package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;

/**
 * Created by codezilla-10 on 21/2/18.
 */

public class CustomPagerAdapter extends PagerAdapter {

    Context context;
    int images[];
    int[] text1;
    int[] text2;


    LayoutInflater layoutInflater;

    public CustomPagerAdapter(Context context, int[] images, int[] text1, int[] text2) {

        this.context = context;
        this.images = images;
        this.text1 = text1;
        this.text2 = text2;


        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemview = layoutInflater.inflate(R.layout.viewpager_item, container, false);
        container.addView(itemview);

        ImageView imageview = (ImageView) itemview.findViewById(R.id.img_viewPager);
        TextView tv_Heading = (TextView) itemview.findViewById(R.id.tv_nameHeading);
        TextView textView1 = (TextView) itemview.findViewById(R.id.tv_viewPager1_main);
        TextView textView2 = (TextView) itemview.findViewById(R.id.tv_viewPager1_sub);


        imageview.setImageResource(images[position]);
        textView1.setText(text1[position]);
        textView2.setText(text2[position]);


        return itemview;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((LinearLayout) object);
    }
}
