package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.models.HomeModel2;

import java.util.ArrayList;

/**
 * Created by codezilla-10 on 26/2/18.
 */

public class HomeFragmentAdapter2 extends RecyclerView.Adapter<HomeFragmentAdapter2.ViewHolder> {
    Context context;
    ArrayList<HomeModel2> homeModelList2;

    public HomeFragmentAdapter2(Context context, ArrayList<HomeModel2> homeModelList2) {
        this.context = context;
        this.homeModelList2 = homeModelList2;
    }

    @Override
    public HomeFragmentAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.new_home_recyclerview2,parent,false);
        return new HomeFragmentAdapter2.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeFragmentAdapter2.ViewHolder holder, int position) {

        final HomeModel2 homeModelposition = homeModelList2.get(position);

        Picasso.with(context)
                .load(homeModelposition.getHomeFragImg2())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return homeModelList2.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.img_widgets2);

        }
    }
}
