package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HistoryRewardAdapter extends RecyclerView.Adapter<HistoryRewardAdapter.RewardHistory> {
    Context context;
    JSONObject object;
    JSONArray innerArray;
    RewardHistoryAdapter rewardHistoryAdapter;

    public HistoryRewardAdapter(Context context, JSONObject object) {
        this.context = context;
        this.object = object;

    }

    @Override
    public RewardHistory onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_bill_history_json, parent, false);
        return new RewardHistory(itemView);    }

    @Override
    public void onBindViewHolder(RewardHistory holder, int position) {
        try {

            JSONArray allKeys = object.names();
            ArrayList ar = new ArrayList();
            HashMap<Object, JSONArray> hashMap = new HashMap<>();

            innerArray = object.getJSONArray((String) allKeys.get(position));
            ar.add(innerArray);

            hashMap.put(allKeys.get(position), innerArray);


            for (int i = 0; i < hashMap.size(); i++) {
                Log.e("KeyName", hashMap.keySet().toArray()[i] + "");
                // Log.e("InHashMapData", hashMap.get(allKeys.get(0))+"");
                holder.tvDate.setText("" + hashMap.keySet().toArray()[i]);
                JSONArray jsonArray = hashMap.get(hashMap.keySet().toArray()[i]);
                Log.e("InHashMapJsonArray", jsonArray + "");

                rewardHistoryAdapter = new RewardHistoryAdapter(context, jsonArray);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                holder.recyclerView.setLayoutManager(linearLayoutManager);
                holder.recyclerView.setAdapter(rewardHistoryAdapter);
                rewardHistoryAdapter.notifyDataSetChanged();


            }

        } catch (Exception e) {
        }
    }

    @Override
    public int getItemCount() {
        return object.length();
    }
    public class RewardHistory extends RecyclerView.ViewHolder {
        TextView tvDate;
        RecyclerView recyclerView;
        public RewardHistory(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tv_date);
            recyclerView = itemView.findViewById(R.id.recyclerview);
        }
    }
}
