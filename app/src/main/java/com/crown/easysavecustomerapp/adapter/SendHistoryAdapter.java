package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by codezilla-121 on 23/3/18.
 */

public class SendHistoryAdapter extends RecyclerView.Adapter<SendHistoryAdapter.Loading> {
    int pos = 0;
    Context context;
    JSONObject object;
    JSONArray innerArray;
    SendAdapterDataData sendAdapterDataData;

    /* public SendHistoryAdapter(Context context, ArrayList<SendReceiveHistory> sendHistoriesList) {
        this.context = context;
        this.sendHistoriesList = sendHistoriesList;

    }*/
    public SendHistoryAdapter(Context context, JSONObject object) {
        this.context = context;
        this.object = object;

    }

    @Override
    public Loading onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_loading_history, parent, false);
        return new Loading(view);
    }

    @Override
    public void onBindViewHolder(Loading holder, int position) {
        try {

            JSONArray allKeys = object.names();
            ArrayList ar = new ArrayList();
            HashMap<Object, JSONArray> hashMap = new HashMap<>();

            innerArray = object.getJSONArray((String) allKeys.get(position));
            ar.add(innerArray);

            hashMap.put(allKeys.get(position), innerArray);


            for (int i = 0; i < hashMap.size(); i++) {
                Log.e("KeyName", hashMap.keySet().toArray()[i] + "");
                // Log.e("InHashMapData", hashMap.get(allKeys.get(0))+"");
                holder.tvDate.setText("" + hashMap.keySet().toArray()[i]);
                JSONArray jsonArray = hashMap.get(hashMap.keySet().toArray()[i]);
                Log.e("InHashMapJsonArray", jsonArray + "");

                sendAdapterDataData = new SendAdapterDataData(context, jsonArray);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                holder.recyclerView.setLayoutManager(linearLayoutManager);
                holder.recyclerView.setAdapter(sendAdapterDataData);
                sendAdapterDataData.notifyDataSetChanged();


            }

        } catch (Exception e) {
        }

    }

    @Override
    public int getItemCount() {
        return object.length();
    }

    public class Loading extends RecyclerView.ViewHolder {
        TextView tvDate;
        RecyclerView recyclerView;

        public Loading(View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.recyclerView);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }
}
