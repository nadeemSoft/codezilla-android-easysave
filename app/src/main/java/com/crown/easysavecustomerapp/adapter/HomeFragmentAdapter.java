package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.PayCompanyActivity;
import com.crown.easysavecustomerapp.models.BillServiceModel;

import java.util.ArrayList;


/**
 * Created by codezilla-10 on 26/2/18.
 */

public class HomeFragmentAdapter extends RecyclerView.Adapter<HomeFragmentAdapter.ViewHolder> {

    Context context;
    ArrayList<BillServiceModel> homeModelList;

    public HomeFragmentAdapter(Context context, ArrayList<BillServiceModel> homeModelList) {
        this.context = context;
        this.homeModelList = homeModelList;
    }

    @Override
    public HomeFragmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_home_recyclerview, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(HomeFragmentAdapter.ViewHolder holder, final int position) {

        final BillServiceModel model = homeModelList.get(position);

        if(position == 0){
            Picasso.with(context)
                    .load(R.drawable.new_mobile)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());


        }else if(position == 1){
            Picasso.with(context)
                    .load(R.drawable.new_education)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());


        }else if(position == 2){
            Picasso.with(context)
                    .load(R.drawable.new_internet)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());


        }else if(position == 3){
            Picasso.with(context)
                    .load(R.drawable.new_utilies)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 4){
            Picasso.with(context)
                    .load(R.drawable.new_insurance)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 5){
            Picasso.with(context)
                    .load(R.drawable.new_cable)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 6){
            Picasso.with(context)
                    .load(R.drawable.new_taxes)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 7){
            Picasso.with(context)
                    .load(R.drawable.new_retail)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 8){
            Picasso.with(context)
                    .load(R.drawable.autohome)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 9){
            Picasso.with(context)
                    .load(R.drawable.homehome)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }



   /*     Picasso.with(context)
                .load(homeModelposition.getHomeFragImg())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.image);

        holder.textView.setText(homeModelposition.getHomeFragText());
*/
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PayCompanyActivity.class);
                intent.putExtra("value",model.getValue()+"");
                Log.e("clickValue",model.getValue()+"");
                context.startActivity(intent);


            }
        });

    }


    @Override
    public int getItemCount() {
        return homeModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView textView;
        LinearLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.img_widgets);
            textView = itemView.findViewById(R.id.textView);
            linearLayout = itemView.findViewById(R.id.linear);
        }
    }
}
