package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.models.Offers;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class WeeklyAdapter extends RecyclerView.Adapter<WeeklyAdapter.offer>{
    ArrayList<Offers> offerlist;
    Context context;
    private int rowLayout;

    WeeklyAdapter ticketsModel;

    public WeeklyAdapter(Context context, int rowLayout, ArrayList<Offers> offerlist) {
        this.context = context;
        this.rowLayout = rowLayout;
        this.offerlist = offerlist;

    }


    @Override
    public offer onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new offer(itemView);    }

    @Override
    public void onBindViewHolder(offer holder, int position) {
       Offers offers  =offerlist.get(position);
        Picasso.with(context)
                .load(offers.getOfferImage())
                .placeholder(R.drawable.icon)
                .error(R.drawable.icon)
                .into(holder.ivImage);
        holder.tvName.setText(offers.getOfferName());
        holder.tvdescription.setText(offers.getOfferDescription());


    }

    @Override
    public int getItemCount() {
        return offerlist.size();
    }

    public class offer extends RecyclerView.ViewHolder {

        TextView tvName, tvdescription;
        ImageView ivImage;
        ConstraintLayout constraintLayout;

        public offer(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_noti_title);
            tvdescription = (TextView) itemView.findViewById(R.id.tv_noti_detail);
            ivImage = (ImageView) itemView.findViewById(R.id.noti_imageView);
            constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.constraint);
        }
    }
}
