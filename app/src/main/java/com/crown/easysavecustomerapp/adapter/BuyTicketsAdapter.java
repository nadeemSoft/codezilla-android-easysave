package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.BuyTicketsActivity;
import com.crown.easysavecustomerapp.models.TicketsModel;

import java.util.ArrayList;

public class BuyTicketsAdapter extends RecyclerView.Adapter<BuyTicketsAdapter.Ticket> {
    ArrayList<TicketsModel> ticketList;
    Context context;
    private int rowLayout;

    TicketsModel ticketsModel;

    public BuyTicketsAdapter(Context context, ArrayList<TicketsModel> ticketList) {
        this.context = context;
        this.ticketList = ticketList;

    }
    public BuyTicketsAdapter(Context context, int rowLayout, ArrayList<TicketsModel> ticketList) {
        this.context = context;
        this.rowLayout = rowLayout;
        this.ticketList = ticketList;

    }

    @Override
    public Ticket onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new Ticket(itemView);
    }

    @Override
    public void onBindViewHolder(Ticket holder, final int position) {
        ticketsModel = ticketList.get(position);
        Picasso.with(context)
                .load(ticketsModel.getImageUrl())
                .placeholder(R.drawable.icon)
                .error(R.drawable.icon)
                .into(holder.ivImage);
        holder.tvName.setText(ticketsModel.getName());
        holder.tvTye.setText(ticketsModel.getCategory());
        holder.tvDoller.setText("$" + ticketsModel.getPrice());
        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BuyTicketsActivity.class);
                intent.putExtra("check","ok");
                intent.putExtra("position",position);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }

    public class Ticket extends RecyclerView.ViewHolder {
        TextView tvName, tvTye, tvDoller;
        ImageView ivImage;
        ConstraintLayout constraintLayout;

        public Ticket(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvTye = (TextView) itemView.findViewById(R.id.tv_type);
            tvDoller = (TextView) itemView.findViewById(R.id.tv_dolller);
            ivImage = (ImageView) itemView.findViewById(R.id.imageView);
            constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.constraint);

        }
    }

}
