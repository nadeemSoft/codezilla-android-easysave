package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.PayCompanyActivity;
import com.crown.easysavecustomerapp.activities.PaySeeAllActivity;
import com.crown.easysavecustomerapp.models.BillServiceModel;

import java.util.ArrayList;

/**
 * Created by codezilla-121 on 21/3/18.
 */

public class PayBillSeeAllAdapter extends RecyclerView.Adapter<PayBillSeeAllAdapter.Item> {
    Context context;
    ArrayList<BillServiceModel> homeModelArrayList;


    public PayBillSeeAllAdapter(Context context, ArrayList<BillServiceModel> homeModelArrayList) {
        this.context = context;
        this.homeModelArrayList = homeModelArrayList;

    }

    @Override
    public PayBillSeeAllAdapter.Item onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pay_see_all,parent,false);
        return new Item(view);
    }

    @Override
    public void onBindViewHolder(PayBillSeeAllAdapter.Item holder, final int position) {
        final BillServiceModel model = homeModelArrayList.get(position);

        if(position == 0){
            Picasso.with(context)
                    .load(R.drawable.new_mobile)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());


        }else if(position == 1){
            Picasso.with(context)
                    .load(R.drawable.new_education)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());


        }else if(position == 2){
            Picasso.with(context)
                    .load(R.drawable.new_internet)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());


        }else if(position == 3){
            Picasso.with(context)
                    .load(R.drawable.new_utilies)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 4){
            Picasso.with(context)
                    .load(R.drawable.new_insurance)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 5){
            Picasso.with(context)
                    .load(R.drawable.new_cable)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 6){
            Picasso.with(context)
                    .load(R.drawable.new_taxes)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 7){
            Picasso.with(context)
                    .load(R.drawable.new_retail)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 8){
            Picasso.with(context)
                    .load(R.drawable.autohome)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }else if(position == 9){
            Picasso.with(context)
                    .load(R.drawable.homehome)
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(holder.image);

            holder.textView.setText(model.getText());

        }
       /* Picasso.with(context)
                .load(homeModelposition.getHomeFragImg())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.image);

        holder.textView.setText(homeModelposition.getHomeFragText());*/
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaySeeAllActivity.paySeeAllActivity, PayCompanyActivity.class);
                intent.putExtra("value",model.getValue()+"");
                Log.e("clickValue",model.getValue()+"");
                PaySeeAllActivity.paySeeAllActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeModelArrayList.size();
    }

    public class Item extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textView;
        LinearLayout linearLayout;

        public Item(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.img_widgets);
            textView = itemView.findViewById(R.id.textView);
            linearLayout = itemView.findViewById(R.id.linear_see_all);
        }
    }
}
