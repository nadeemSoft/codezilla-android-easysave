package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MessageNotification> {
    Context context;
    ArrayList list;

    public NotificationAdapter(Context context, ArrayList list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public MessageNotification onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification, parent, false);
        return new MessageNotification(itemView);
    }

    @Override
    public void onBindViewHolder(MessageNotification holder, int position) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MessageNotification extends RecyclerView.ViewHolder {
        TextView tvNotificationTitle, tvNotificationDetails;
        ImageView imageView;

        public MessageNotification(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.noti_imageView);
            tvNotificationTitle = itemView.findViewById(R.id.tv_noti_title);
            tvNotificationDetails = itemView.findViewById(R.id.tv_noti_detail);

        }
    }
}
