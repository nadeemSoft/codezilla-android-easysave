package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.models.SendReceiveHistory;

import java.util.ArrayList;

/**
 * Created by codezilla-121 on 23/3/18.
 */

public class RecieverHistoryAdapter extends RecyclerView.Adapter<RecieverHistoryAdapter.Receiver> {
    int pos = 0;
    Context context;
    ArrayList<SendReceiveHistory> receiveHistoriesList;

    public RecieverHistoryAdapter(Context context, ArrayList<SendReceiveHistory> receiveHistoriesList) {
        this.context = context;
        this.receiveHistoriesList = receiveHistoriesList;

    }

    @Override
    public Receiver onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_loading_history, parent, false);
        return new Receiver(view);
    }

    @Override
    public void onBindViewHolder(Receiver holder, int position) {
        SendReceiveHistory sendReceiveHistory = receiveHistoriesList.get(position);
        String date = sendReceiveHistory.getTransDate();
        if (date.equalsIgnoreCase(sendReceiveHistory.getTransDate()) && position == pos) {
            holder.tvDate.setVisibility(View.VISIBLE);
            holder.tvDate.setText(date+"");


        }else {
            holder.tvDate.setVisibility(View.GONE);
            pos++;

        }
/*
        Picasso.with(context)
                .load(R.drawable.load_history)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.iv_image);*/
        holder.tv_name.setText(sendReceiveHistory.getSenderName());
        holder.tv_time.setText(sendReceiveHistory.getTransTime());
        // holder.tv_date.setText(sendReceiveHistory.getTransDate());
        holder.tv_money.setText(sendReceiveHistory.getAmount());

    }

    @Override
    public int getItemCount() {
        return receiveHistoriesList.size();
    }

    public class Receiver extends RecyclerView.ViewHolder {

        TextView tv_name, tv_time, tv_money, tvDate;
        ImageView iv_image, iv_status_image;

        public Receiver(View itemView) {
            super(itemView);

            iv_image = itemView.findViewById(R.id.ivround_loading_image);
            iv_status_image = itemView.findViewById(R.id.iv_status_image);
            tv_name = itemView.findViewById(R.id.tv_loading_name);
            tv_time = itemView.findViewById(R.id.tv_loading_time);
            tv_money = itemView.findViewById(R.id.tv_loading_money);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }
}
