package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.models.RewardModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by codezilla-10 on 27/2/18.
 */

public class RewardHistoryAdapter extends RecyclerView.Adapter<RewardHistoryAdapter.ViewHolder> {

    Context context;
    int pos = 0;
    ArrayList<RewardModel> rewardDataList;
    int ic = 0;
    JSONArray jsonArray;

    /*public RewardHistoryAdapter(Context context, ArrayList<RewardModel> rewardDataList) {
        this.context = context;
        this.rewardDataList = rewardDataList;
    }*/


    public RewardHistoryAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;
    }


    @Override
    public RewardHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reward_adapter, parent, false);
        return new RewardHistoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RewardHistoryAdapter.ViewHolder holder, int position) {
        try {
            holder.linerRewardHide.setVisibility(View.GONE);

            JSONObject object = jsonArray.getJSONObject(position);
            holder.tvRewardName.setText(object.getString("merchantName"));
            holder.tvRewardMoney.setText(object.getString("reward"));
            holder.tvTime.setText(object.getString("transTime"));
          //  holder.tvSpendMoney.setText(object.getString("$" + "amount"));


        } catch (Exception e) {

        }

        holder.ivDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ic == 0) {
                    holder.linerRewardHide.setVisibility(View.VISIBLE);
                    ic++;

                } else {
                    holder.linerRewardHide.setVisibility(View.GONE);
                    ic--;


                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvRewardName, tvTime, tvSpendMoney, tvRewardMoney, tvDate;
        ImageView ivDown;
        LinearLayout linerRewardHide;

        public ViewHolder(View itemView) {
            super(itemView);
            ivDown = itemView.findViewById(R.id.iv_reward_down);
            tvRewardName = itemView.findViewById(R.id.tv_reward_name);
            tvTime = itemView.findViewById(R.id.tv_reward_time);
            tvSpendMoney = itemView.findViewById(R.id.tv_spend_money);
            tvRewardMoney = itemView.findViewById(R.id.tv_reward_money);
            linerRewardHide = itemView.findViewById(R.id.linear_reward_hide);
            tvDate = itemView.findViewById(R.id.tv_date);

        }
    }
}
