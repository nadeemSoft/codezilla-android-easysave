package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.models.MerchantModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class EarnCashBackAdapterNew extends RecyclerView.Adapter<EarnCashBackAdapterNew.Earn> {
    Context context;
    ArrayList<MerchantModel> merchantList;
    private int rowLayout;

    public EarnCashBackAdapterNew(Context context, int rowLayout, ArrayList<MerchantModel> merchantList) {
        this.context = context;
        this.rowLayout = rowLayout;
        this.merchantList = merchantList;

    }

    @Override
    public Earn onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new Earn(itemView);
    }

    @Override
    public void onBindViewHolder(Earn holder, int position) {

        MerchantModel merchantModel = merchantList.get(position);
        Picasso.with(context)
                .load(merchantModel.getImageUrl())
                .placeholder(R.drawable.icon)
                .error(R.drawable.icon)
                .into(holder.ivImage);
        holder.tvName.setText(merchantModel.getMerchantName());

    }

    @Override
    public int getItemCount() {
        return merchantList.size();
    }

    public class Earn extends RecyclerView.ViewHolder {
        TextView tvName, tvTye, tvDoller;
        ImageView ivImage;
        ConstraintLayout constraintLayout;

        public Earn(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            ivImage = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }
}
