package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.models.BillHistoryModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class BillAdapterHistory extends RecyclerView.Adapter<BillAdapterHistory.BillsHistory> {
    Context context;
    ArrayList<BillHistoryModel> billHistoryList = null;
    JSONObject object;
    JSONArray innerArray;
    BIllHistoryDateData bIllHistoryDateData;
    int pos = 0;

    public BillAdapterHistory(Context context, ArrayList<BillHistoryModel> billHistoryList) {
        this.context = context;
        this.billHistoryList = billHistoryList;

    }

    public BillAdapterHistory(Context context, JSONObject object) {
        this.context = context;
        this.object = object;

    }

    @Override
    public void onBindViewHolder(BillsHistory holder, int position) {

        try {

            JSONArray allKeys = object.names();
            ArrayList ar = new ArrayList();
            HashMap<Object, JSONArray> hashMap = new HashMap<>();

            innerArray = object.getJSONArray((String) allKeys.get(position));
            ar.add(innerArray);

            hashMap.put(allKeys.get(position), innerArray);


            for (int i = 0; i < hashMap.size(); i++) {
                Log.e("KeyName", hashMap.keySet().toArray()[i] + "");
                // Log.e("InHashMapData", hashMap.get(allKeys.get(0))+"");
                holder.tvDate.setText("" + hashMap.keySet().toArray()[i]);
                JSONArray jsonArray = hashMap.get(hashMap.keySet().toArray()[i]);
                Log.e("InHashMapJsonArray", jsonArray + "");
                bIllHistoryDateData = new BIllHistoryDateData(context, jsonArray);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                holder.recyclerView.setLayoutManager(linearLayoutManager);
                holder.recyclerView.setAdapter(bIllHistoryDateData);
                bIllHistoryDateData.notifyDataSetChanged();


            }

        } catch (Exception e) {
        }


    }

    @Override
    public BillsHistory onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_bill_history_json, parent, false);
        return new BillsHistory(itemView);
    }

    @Override
    public int getItemCount() {
        return object.length();
    }

    public class BillsHistory extends RecyclerView.ViewHolder {

        TextView tvDate;
        RecyclerView recyclerView;

        public BillsHistory(View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.tv_date);
            recyclerView = itemView.findViewById(R.id.recyclerview);


        }
    }
}
