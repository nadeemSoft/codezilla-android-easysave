package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class HistoryLoadingDataData extends RecyclerView.Adapter<HistoryLoadingDataData.LoadingData> {
    Context context;
    JSONArray jsonArray;

    public HistoryLoadingDataData(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;

    }

    @Override
    public LoadingData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_history_data_data, parent, false);
        return new LoadingData(itemView);
    }

    @Override
    public void onBindViewHolder(LoadingData holder, int position) {
        try {
            JSONObject object = jsonArray.getJSONObject(position);

           // holder.tvName.setText( object.getString("companyName"));
            holder.tvMoney.setText("$"+object.getString("amount"));
            holder.tvTime.setText( object.getString("transTime"));

        }catch (Exception e){

        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public class LoadingData extends RecyclerView.ViewHolder {
        TextView tvName, tvTime, tvMoney, tvDate;
        ImageView ivImage, ivStatus;
        public LoadingData(View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivround_loading_image);
            ivStatus = itemView.findViewById(R.id.iv_status_image);
            tvName = itemView.findViewById(R.id.tv_loading_name);
            tvTime = itemView.findViewById(R.id.tv_loading_time);
            tvMoney = itemView.findViewById(R.id.tv_loading_money);
            tvDate = itemView.findViewById(R.id.tv_date);
        }
    }
}
