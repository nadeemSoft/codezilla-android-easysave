package com.crown.easysavecustomerapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.PayAbillActivity;
import com.crown.easysavecustomerapp.activities.PayCompanyActivity;
import com.crown.easysavecustomerapp.models.BillerCompany;

import java.util.ArrayList;

/**
 * Created by codezilla-121 on 22/3/18.
 */

public class PayCompanyAdapter extends RecyclerView.Adapter<PayCompanyAdapter.Company> {
    Context context;
    ArrayList<BillerCompany> billerList;
    String billerId = null;

    public PayCompanyAdapter(Context context, ArrayList<BillerCompany> billerList) {
        this.context = context;
        this.billerList = billerList;

    }

    @Override
    public Company onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pay_company, parent, false);
        return new Company(view);
    }

    @Override
    public void onBindViewHolder(Company holder, int position) {
        final BillerCompany billerCompany = billerList.get(position);
        String image = billerCompany.getBillerImage();


        Picasso.with(context)
                .load(billerCompany.getBillerImage())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.ivCompany);

        holder.tvCompanyName.setText(billerCompany.getBillerCompanyName());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PayAbillActivity.class);
                intent.putExtra("billerId", billerCompany.getBillerId() + "");
                intent.putExtra("billerImage", billerCompany.getBillerImage() + "");

                Log.e("billerId", billerCompany.getBillerId());
                Log.e("billerImage", billerCompany.getBillerImage());

                PayCompanyActivity.payCompanyActivity.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return billerList.size();
    }

    public class Company extends RecyclerView.ViewHolder {
        ImageView ivCompany;
        TextView tvCompanyName;
        CardView cardView;

        public Company(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cv);
            ivCompany = (ImageView) itemView.findViewById(R.id.iv_adapter_pay_company);
            tvCompanyName= (TextView) itemView.findViewById(R.id.tv_adapter_pay_company);
        }
    }
}
