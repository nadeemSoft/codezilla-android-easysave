package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

/**
 * Created by codezilla-121 on 23/3/18.
 */

public class SendReceiveHistory implements Serializable{
    String amount,recieverName,senderName,transDate,transTime;

    public SendReceiveHistory(String amount, String recieverName, String senderName, String transDate, String transTime) {
        this.amount = amount;
        this.recieverName = recieverName;
        this.senderName = senderName;
        this.transDate = transDate;
        this.transTime = transTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRecieverName() {
        return recieverName;
    }

    public void setRecieverName(String recieverName) {
        this.recieverName = recieverName;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }
}
