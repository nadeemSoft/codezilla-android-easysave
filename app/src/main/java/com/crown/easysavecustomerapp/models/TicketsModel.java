package com.crown.easysavecustomerapp.models;

import org.json.JSONArray;

import java.io.Serializable;

public class TicketsModel implements Serializable {
    String Category, Details,EndDate, ImageUrl, Name, Price, TicketId, TicketsAvaliable;
    JSONArray jsonArray;

    public TicketsModel(String category, String details, String endDate, String imageUrl, String name, String price, String ticketId, String ticketsAvaliable, JSONArray jsonArray) {
        Category = category;
        Details = details;
        EndDate = endDate;
        ImageUrl = imageUrl;
        Name = name;
        Price = price;
        TicketId = ticketId;
        TicketsAvaliable = ticketsAvaliable;
        this.jsonArray = jsonArray;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getTicketId() {
        return TicketId;
    }

    public void setTicketId(String ticketId) {
        TicketId = ticketId;
    }

    public String getTicketsAvaliable() {
        return TicketsAvaliable;
    }

    public void setTicketsAvaliable(String ticketsAvaliable) {
        TicketsAvaliable = ticketsAvaliable;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }
}
