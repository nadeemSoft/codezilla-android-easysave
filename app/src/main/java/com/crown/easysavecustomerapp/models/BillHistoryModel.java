package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

public class BillHistoryModel implements Serializable{
    String amount,companyName,transDate,transTime;

    public BillHistoryModel(String amount, String companyName, String transDate, String transTime) {
        this.amount = amount;
        this.companyName = companyName;
        this.transDate = transDate;
        this.transTime = transTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }
}
