package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

/**
 * Created by codezilla-121 on 24/3/18.
 */

public class RewardModel implements Serializable {

    String amount,merchantName,reward,transDate,transTime;


    public RewardModel(String amount, String merchantName, String reward, String transDate, String transTime) {
        this.amount = amount;
        this.merchantName = merchantName;
        this.reward = reward;
        this.transDate = transDate;
        this.transTime = transTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransTime() {
        return transTime;
    }

    public void setTransTime(String transTime) {
        this.transTime = transTime;
    }
}
