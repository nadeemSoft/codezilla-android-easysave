package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

/**
 * Created by codezilla-121 on 22/3/18.
 */

public class BillerCompany implements Serializable{
    String billerImage,billerId,billeCategory,billerCompanyName;

    public BillerCompany(String billerImage, String billerId, String billeCategory, String billerCompanyName) {
        this.billerImage = billerImage;
        this.billerId = billerId;
        this.billeCategory = billeCategory;
        this.billerCompanyName = billerCompanyName;
    }

    public String getBillerImage() {
        return billerImage;
    }

    public void setBillerImage(String billerImage) {
        this.billerImage = billerImage;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getBilleCategory() {
        return billeCategory;
    }

    public void setBilleCategory(String billeCategory) {
        this.billeCategory = billeCategory;
    }

    public String getBillerCompanyName() {
        return billerCompanyName;
    }

    public void setBillerCompanyName(String billerCompanyName) {
        this.billerCompanyName = billerCompanyName;
    }
}
