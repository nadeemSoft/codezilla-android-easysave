package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

public class MerchantModel implements Serializable{
    String imageUrl,merchantId,MerchantName;

    public MerchantModel(String imageUrl, String merchantId, String merchantName) {
        this.imageUrl = imageUrl;
        this.merchantId = merchantId;
        MerchantName = merchantName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return MerchantName;
    }

    public void setMerchantName(String merchantName) {
        MerchantName = merchantName;
    }
}
