package com.crown.easysavecustomerapp.models;

public class Offers {
    String offerImage,offerName,offerId,offerDescription;

    public Offers(String offerImage, String offerName, String offerId, String offerDescription) {
        this.offerImage = offerImage;
        this.offerName = offerName;
        this.offerId = offerId;
        this.offerDescription = offerDescription;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public void setOfferImage(String offerImage) {
        this.offerImage = offerImage;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }
}
