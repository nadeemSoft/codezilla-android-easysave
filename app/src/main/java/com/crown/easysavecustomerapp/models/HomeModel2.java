package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

/**
 * Created by codezilla-10 on 26/2/18.
 */

public class HomeModel2 implements Serializable {

    int homeFragImg2;

    public HomeModel2(int homeFragImg2) {
        this.homeFragImg2 = homeFragImg2;
    }

    public int getHomeFragImg2() {
        return homeFragImg2;
    }

    public void setHomeFragImg2(int homeFragImg2) {
        this.homeFragImg2 = homeFragImg2;
    }
}
