package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

public class BookedTicketModel implements Serializable{
    String BookedDate,BookedTimeSlot,Category,Details,ImageUrl,Name,NoOfTickets,TicketId;

    public BookedTicketModel(String bookedDate, String bookedTimeSlot, String category, String details, String imageUrl, String name, String noOfTickets, String ticketId) {
        BookedDate = bookedDate;
        BookedTimeSlot = bookedTimeSlot;
        Category = category;
        Details = details;
        ImageUrl = imageUrl;
        Name = name;
        NoOfTickets = noOfTickets;
        TicketId = ticketId;
    }

    public String getBookedDate() {
        return BookedDate;
    }

    public void setBookedDate(String bookedDate) {
        BookedDate = bookedDate;
    }

    public String getBookedTimeSlot() {
        return BookedTimeSlot;
    }

    public void setBookedTimeSlot(String bookedTimeSlot) {
        BookedTimeSlot = bookedTimeSlot;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNoOfTickets() {
        return NoOfTickets;
    }

    public void setNoOfTickets(String noOfTickets) {
        NoOfTickets = noOfTickets;
    }

    public String getTicketId() {
        return TicketId;
    }

    public void setTicketId(String ticketId) {
        TicketId = ticketId;
    }
}