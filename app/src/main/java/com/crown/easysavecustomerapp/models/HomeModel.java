package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

/**
 * Created by codezilla-10 on 26/2/18.
 */

public class HomeModel implements Serializable{

    public HomeModel(int homeFragImg, String homeFragText) {
        this.homeFragImg = homeFragImg;
        this.homeFragText = homeFragText;
    }

    String homeFragText;
    int homeFragImg;

    public int getHomeFragImg() {
        return homeFragImg;
    }

    public void setHomeFragImg(int homeFragImg) {
        this.homeFragImg = homeFragImg;
    }

    public String getHomeFragText() {
        return homeFragText;
    }

    public void setHomeFragText(String homeFragText) {
        this.homeFragText = homeFragText;
    }
}
