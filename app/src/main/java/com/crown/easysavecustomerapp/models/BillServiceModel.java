package com.crown.easysavecustomerapp.models;

import java.io.Serializable;

public class BillServiceModel implements Serializable {
    String text,value;

    public BillServiceModel(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
