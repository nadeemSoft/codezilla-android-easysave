package com.crown.easysavecustomerapp.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.NavigationDrawerActivity;
import com.crown.easysavecustomerapp.adapter.NotificationAdapter;
import com.crown.easysavecustomerapp.adapter.WeeklyAdapter;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.models.Offers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationFragment extends Fragment {
    RecyclerView rvNotificaton;
    TextView tvNoRecord;
    NotificationAdapter notificationAdapter;
    ArrayList<Offers> offerlist = null;
    ProgressDialog pd;
    WeeklyAdapter weeklyAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        getActivity().setTitle("Alerts");
        rvNotificaton = (RecyclerView) view.findViewById(R.id.rv_offers);
        tvNoRecord = (TextView) view.findViewById(R.id.tv_no_record);
        offerlist = new ArrayList();
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getOffers();
        setBottomColore();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    HomeFragment homeFragment = new HomeFragment();
                    android.support.v4.app.FragmentTransaction transaction = ((NavigationDrawerActivity) getContext()).getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, homeFragment); // fragmen container id in first parameter is the  container(Main layout id) of Activity
                    transaction.commit();

                    return true;
                }
                return false;
            }
        });
    }

    private void getOffers() {
        pd.show();


        //http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/offers?key=EASYSAVECUSTOMERAPIV1-10001
        String url = AppConstant.BASE_URL_GET + "offers?key=" + AppConstant.KEY;
        Log.e("chekurl", url);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response + "");
                offerlist.clear();
                try {

                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");

                    if (errorCode.equals("0")) {
                        JSONArray objReciev = objResponse.getJSONArray("offers");
                        Log.e("OfferResponse", objReciev + "");

                        for (int i = 0; i < objReciev.length(); i++) {
                            JSONObject objReceivedata = objReciev.getJSONObject(i);
                            String Description = objReceivedata.getString("Description");
                            String Name = objReceivedata.getString("Name");
                            String OfferId = objReceivedata.getString("OfferId");
                            String OfferImage = objReceivedata.getString("OfferImage");

                            Offers offers = new Offers(OfferImage, Name, OfferId, Description);
                            offerlist.add(offers);
                        }

                        weeklyAdapter = new WeeklyAdapter(getActivity(), R.layout.adapter_notification, offerlist);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        rvNotificaton.setLayoutManager(linearLayoutManager);
                        rvNotificaton.setAdapter(weeklyAdapter);

                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(getActivity(), errormessage, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(getActivity(), "Exception" + e, Toast.LENGTH_SHORT).show();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(getActivity(), "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }


    private void setBottomColore() {

        NavigationDrawerActivity.ivHome.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab1_inactive));
        NavigationDrawerActivity.tvHome.setTextColor(getResources().getColor(R.color.dark_grey));

        NavigationDrawerActivity.ivWallet.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab4_inactive));
        NavigationDrawerActivity.tvWallet.setTextColor(getResources().getColor(R.color.dark_grey));

        NavigationDrawerActivity.ivProfile.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab3_inactive));
        NavigationDrawerActivity.tvProfile.setTextColor(getResources().getColor(R.color.dark_grey));

        NavigationDrawerActivity.ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.ic_activenotification));
        NavigationDrawerActivity.tvNotification.setTextColor(getResources().getColor(R.color.active_Bottom));
    }
}
