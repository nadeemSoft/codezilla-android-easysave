package com.crown.easysavecustomerapp.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.LoadMoneyActivity;
import com.crown.easysavecustomerapp.activities.NavigationDrawerActivity;
import com.crown.easysavecustomerapp.activities.OurPartnersActivity;
import com.crown.easysavecustomerapp.activities.PaySeeAllActivity;
import com.crown.easysavecustomerapp.activities.SendMoneyActivity;
import com.crown.easysavecustomerapp.activities.TicketsActivity;
import com.crown.easysavecustomerapp.adapter.BuyTicketsAdapter;
import com.crown.easysavecustomerapp.adapter.EarnCashBackAdapter;
import com.crown.easysavecustomerapp.adapter.EarnCashBackAdapterNew;
import com.crown.easysavecustomerapp.adapter.HomeFragmentAdapter;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.models.BillServiceModel;
import com.crown.easysavecustomerapp.models.HomeModel;
import com.crown.easysavecustomerapp.models.HomeModel2;
import com.crown.easysavecustomerapp.models.MerchantModel;
import com.crown.easysavecustomerapp.models.TicketsModel;
import com.crown.easysavecustomerapp.utils.SessionManager;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by codezilla-10 on 26/2/18.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    public static ArrayList<HomeModel> homeModelArrayList = new ArrayList<>();
    public static ArrayList<BillServiceModel> billServicList = null;
    ArrayList<HomeModel2> homeModel2ArrayList = new ArrayList<>();
    public static ArrayList<TicketsModel> buyTicketsModelList = null;
    public static ArrayList<MerchantModel> earnCashBackList = null;
    private Context context = null;


    ImageView imageQr;
    TextView tvMyCard, tvPayBill, tvGetCashBack, tvSeeAll, tvMyCardNo, tvSeeAllBuyTicket,tvEarnSeeAll;
    RecyclerView rvPayBill, rvGetCashBack, rvBuyATicket;
    LinearLayout llBuyATicketHomeText = null,llPayBillText = null,llEarnCashBack = null;
    HomeFragmentAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    Button btnSendMoney, btnLoadMoney;
    ProgressDialog pd = null;
    View rootview;
    SessionManager sessionManager;
    BuyTicketsAdapter buyTicketsAdapter;
    EarnCashBackAdapterNew earnCashBackAdapter;
    Bitmap myBitmap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.new_home_fragment, container, false);
        getActivity().setTitle("Home");
        sessionManager = new SessionManager(getActivity());
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        billServicList = new ArrayList<>();
        buyTicketsModelList = new ArrayList<>();
        earnCashBackList = new ArrayList<>();
        findWidgets();
        context = getContext();

        String cardId = sessionManager.getKeyCardNumber();
          myBitmap = QRCode.from(cardId).bitmap();

        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        setRecyclerPayBillData();
        getEarnCashBack();
        getTickets();
        setBottomColore();
        imageQr.setImageBitmap(myBitmap);
        tvMyCard.setText("" + sessionManager.getKeyCardNumber());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendMoney:
                Intent intentSend = new Intent(getActivity(), SendMoneyActivity.class);
                startActivity(intentSend);
                break;
            case R.id.loadMoney:
                Intent intentLoad = new Intent(getActivity(), LoadMoneyActivity.class);
                startActivity(intentLoad);
                break;
            case R.id.tv_ViewAll1:
                Intent intentSeeAll = new Intent(getActivity(), PaySeeAllActivity.class);
                startActivity(intentSeeAll);
                break;
            case R.id.tv_see_all_buyTicket:
                Intent intent = new Intent(getActivity(), TicketsActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_earn_ViewAll:
                Intent intentEarn = new Intent(getActivity(), OurPartnersActivity.class);
                startActivity(intentEarn);
                break;
        }

    }

    private void findWidgets() {

        imageQr = rootview.findViewById(R.id.img_qr);
        tvMyCard = rootview.findViewById(R.id.tv_mycard);
        tvPayBill = rootview.findViewById(R.id.tv_paybill);
        tvGetCashBack = rootview.findViewById(R.id.tv_getCashback);
        tvSeeAll = rootview.findViewById(R.id.tv_ViewAll1);
        tvEarnSeeAll = rootview.findViewById(R.id.tv_earn_ViewAll);
        tvEarnSeeAll.setOnClickListener(this);
        tvSeeAll.setOnClickListener(this);
        tvSeeAllBuyTicket = rootview.findViewById(R.id.tv_see_all_buyTicket);
        tvSeeAllBuyTicket.setOnClickListener(this);
        rvPayBill = rootview.findViewById(R.id.rv_pay_bill);
        tvMyCard = rootview.findViewById(R.id.tv_mycard_no);
        rvGetCashBack = rootview.findViewById(R.id.rv_cash_back);
        rvBuyATicket = rootview.findViewById(R.id.rv_buy_ticket);
        btnLoadMoney = rootview.findViewById(R.id.loadMoney);
        btnSendMoney = rootview.findViewById(R.id.sendMoney);

        llBuyATicketHomeText = rootview.findViewById(R.id.ll_buy_a_ticket);
        llEarnCashBack = rootview.findViewById(R.id.ll_earn_cash_back);
        llPayBillText = rootview.findViewById(R.id.ll_pay_bill_text);

        btnLoadMoney.setOnClickListener(this);
        btnLoadMoney.setEnabled(true);
        btnSendMoney.setOnClickListener(this);
    }


    private void setRecyclerPayBillData() {
        pd.show();
        homeModelArrayList.clear();
        billServicList.clear();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest request = new StringRequest(Request.Method.GET, AppConstant.BILL_SERVICES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject objResponse = new JSONObject(String.valueOf(response));
                    Log.e("billServiceResponse", objResponse + "");

                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errorMessage = objHeader.optString("ErrorMessage");

                    if (errorCode.equals("0")) {

                        llPayBillText.setVisibility(View.VISIBLE);
                        rvPayBill.setVisibility(View.GONE);
                        JSONArray arr = objResponse.getJSONArray("services");

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj = arr.getJSONObject(i);
                            String Text = obj.getString("Text");
                            String Value = obj.getString("Value");
                            BillServiceModel billServiceModel = new BillServiceModel(Text, Value);
                            billServicList.add(billServiceModel);
                        }


                        rvPayBill.setHasFixedSize(true);
                        adapter = new HomeFragmentAdapter(getActivity(), billServicList);
                        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        rvPayBill.setLayoutManager(linearLayoutManager);
                        rvPayBill.setAdapter(adapter);
                        imageQr.setImageBitmap(myBitmap);
                        tvMyCard.setText("" + sessionManager.getKeyCardNumber());
                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        llPayBillText.setVisibility(View.VISIBLE);
                        rvPayBill.setVisibility(View.GONE);
                        //Toast.makeText(getActivity(), "" + errorMessage, Toast.LENGTH_LONG).show();

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    Log.e("ExceptionBillServiceIn", e + "");

                    Toast.makeText(getActivity(), "Exception" + e, Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();

                Toast.makeText(getActivity(), "VolleyError" + error, Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(request);


    }


    private void getTickets() {
        pd.show();

        //String url = "http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/tickets?key=EASYSAVECUSTOMERAPIV1-10001";
        String url = AppConstant.BASE_URL_GET + "tickets?key=" + AppConstant.KEY;
        Log.e("chekurl", url);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("responseTicketInHOme", response + "");
                buyTicketsModelList.clear();
                Arrays arrays[];
                try {

                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");

                    if (errorCode.equals("0")) {
                        llBuyATicketHomeText.setVisibility(View.GONE);
                        rvBuyATicket.setVisibility(View.VISIBLE);
                        JSONArray objReciev = objResponse.getJSONArray("tickets");
                        for (int i = 0; i < objReciev.length(); i++) {
                            JSONObject objReceivedata = objReciev.getJSONObject(i);
                            String Category = objReceivedata.getString("Category");
                            String Details = objReceivedata.getString("Details");
                            String EndDate = objReceivedata.getString("EndDate");
                            String ImageUrl = objReceivedata.getString("ImageUrl");
                            String Name = objReceivedata.getString("Name");
                            String Price = objReceivedata.getString("Price");
                            String TicketId = objReceivedata.getString("TicketId");
                            String TicketsAvaliable = objReceivedata.getString("TicketsAvaliable");
                            JSONArray TimeSlotsJsonArray = objReceivedata.getJSONArray("TimeSlots");


                            TicketsModel ticketsModel = new TicketsModel(Category, Details, EndDate, ImageUrl, Name, Price, TicketId, TicketsAvaliable, TimeSlotsJsonArray);
                            buyTicketsModelList.add(ticketsModel);
                        }

                        buyTicketsAdapter = new BuyTicketsAdapter(context, R.layout.adapter_buy_ticket_home, buyTicketsModelList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        rvBuyATicket.setLayoutManager(linearLayoutManager);
                        rvBuyATicket.setAdapter(buyTicketsAdapter);

                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        llBuyATicketHomeText.setVisibility(View.VISIBLE);
                        rvBuyATicket.setVisibility(View.GONE);
                       // Toast.makeText(context, errormessage, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("ExceptionTicketInHOme", e + "");
                    Toast.makeText(context, "Exception" + e, Toast.LENGTH_SHORT).show();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(context, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }


    private void getEarnCashBack() {
        pd.show();

        // http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/merchnats?key=EASYSAVECUSTOMERAPIV1-10001

        String url = AppConstant.BASE_URL_GET + "merchnats?key=" + AppConstant.KEY;
        Log.e("chekurl", url);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("responseEarnCashMack", response + "");
                earnCashBackList.clear();
                Arrays arrays[];
                try {

                    JSONObject objResponse = new JSONObject(response);

                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");

                    if (errorCode.equals("0")) {
                        llEarnCashBack.setVisibility(View.GONE);
                        rvGetCashBack.setVisibility(View.VISIBLE);
                        JSONArray objReciev = objResponse.getJSONArray("merchants");
                        Log.e("merchantsResponse", objReciev + "");

                        for (int i = 0; i < objReciev.length(); i++) {
                            JSONObject objReceivedata = objReciev.getJSONObject(i);
                            String Image = objReceivedata.getString("Image");
                            String MerchantId = objReceivedata.getString("MerchantId");
                            String Name = objReceivedata.getString("Name");

                            MerchantModel merchantModel = new MerchantModel(Image, MerchantId, Name);
                            earnCashBackList.add(merchantModel);
                        }

                        earnCashBackAdapter = new EarnCashBackAdapterNew(getContext(),R.layout.adapter_earn_cash_back_new, earnCashBackList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

                        //rvGetCashBack.setLayoutManager(new GridLayoutManager(getActivity(), 3));

                        rvGetCashBack.setLayoutManager(linearLayoutManager);
                        rvGetCashBack.setNestedScrollingEnabled(false);
                        rvGetCashBack.setAdapter(earnCashBackAdapter);

                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        llEarnCashBack.setVisibility(View.VISIBLE);
                        rvGetCashBack.setVisibility(View.GONE);
                       // Toast.makeText(getActivity(), errormessage, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("ExceptionEarnCasInHOme", e + "");

                    Toast.makeText(getActivity(), "Exception" + e, Toast.LENGTH_SHORT).show();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(getActivity(), "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }

    private void setBottomColore() {

        NavigationDrawerActivity.ivHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_homeactive));
        NavigationDrawerActivity.tvHome.setTextColor(getResources().getColor(R.color.active_Bottom));

        NavigationDrawerActivity.ivWallet.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab2_inactive));
        NavigationDrawerActivity.tvWallet.setTextColor(getResources().getColor(R.color.dark_grey));

        NavigationDrawerActivity.ivProfile.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab3_inactive));
        NavigationDrawerActivity.tvProfile.setTextColor(getResources().getColor(R.color.dark_grey));

        NavigationDrawerActivity.ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab4_inactive));
        NavigationDrawerActivity.tvNotification.setTextColor(getResources().getColor(R.color.dark_grey));
    }


}
