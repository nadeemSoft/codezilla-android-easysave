package com.crown.easysavecustomerapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.SendReceiveHistoryActivity;
import com.crown.easysavecustomerapp.adapter.BillAdapterHistory;
import com.crown.easysavecustomerapp.adapter.SendHistoryAdapter;

/**
 * Created by codezilla-10 on 23/2/18.
 */

public class SendHistoryFragment extends Fragment {
    RecyclerView rvLoading;
    LinearLayout linearNoHistory;
    BillAdapterHistory billAdapterHistory;
    SendHistoryAdapter sendHistoryAdapter;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.send_history_layout, container, false);
        rvLoading = (RecyclerView)view.findViewById(R.id.rv_loading);
        linearNoHistory = (LinearLayout)view.findViewById(R.id.linear_no_history);
        if(SendReceiveHistoryActivity.jsonObjDateSend.length() != 0){
            sendHistoryAdapter = new SendHistoryAdapter(getContext(), SendReceiveHistoryActivity.jsonObjDateSend);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvLoading.setLayoutManager(linearLayoutManager);
            rvLoading.setAdapter(sendHistoryAdapter);

          /*  billAdapterHistory = new BillAdapterHistory(getContext(),SendReceiveHistoryActivity.jsonObjDateSend);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvLoading.setLayoutManager(linearLayoutManager);
            rvLoading.setAdapter(billAdapterHistory);
            billAdapterHistory.notifyDataSetChanged();*/


        }else {
            rvLoading.setVisibility(View.GONE);
            linearNoHistory.setVisibility(View.VISIBLE);


        }

        return view;
    }
}
