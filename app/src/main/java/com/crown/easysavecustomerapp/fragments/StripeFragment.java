package com.crown.easysavecustomerapp.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.StripeActivity;
import com.crown.easysavecustomerapp.utils.SessionManager;

public class StripeFragment extends Fragment implements View.OnClickListener{
    int amount;
    SessionManager sessionManager;

    private Button buttonPay;
    private EditText editTextAmount;
    ProgressDialog pd;


    //Payment Amount
    private String paymentAmount;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paypal, container, false);
      //  amount = sessionManager.getKeyTotalAmount();
        amount = Integer.parseInt(sessionManager.getActualAmount());

        pd = new ProgressDialog(getContext());
        pd.setMessage("Loading Point...");
        pd.setCancelable(false);
        buttonPay = (Button) view.findViewById(R.id.buttonPay);
        editTextAmount = (EditText) view.findViewById(R.id.editTextAmount);
        editTextAmount.setText("" + amount);
        Log.e("AmountStripe",amount+"");

        buttonPay.setOnClickListener(this);
        return view;


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.buttonPay:
                Intent intent = new Intent(getActivity(), StripeActivity.class);
                intent.putExtra("amount",amount);
                startActivity(intent);
                break;
        }

    }
}
