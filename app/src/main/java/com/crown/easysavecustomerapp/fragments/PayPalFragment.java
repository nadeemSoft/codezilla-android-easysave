/*
package com.crown.easysavecustomerapp.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.ConfirmationActivity;
import com.crown.easysavecustomerapp.utils.PayPalConfig;
import com.crown.easysavecustomerapp.utils.SessionManager;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

public class PayPalFragment extends Fragment implements View.OnClickListener {
    int amount;
    SessionManager sessionManager;

    private Button buttonPay;
    private EditText editTextAmount;
    ProgressDialog pd;


    //Payment Amount
    private String paymentAmount;
    public static final int PAYPAL_REQUEST_CODE = 123;

    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paypal, container, false);
        amount = sessionManager.getKeyTotalAmount();
        pd = new ProgressDialog(getContext());
        pd.setMessage("Loading Point...");
        pd.setCancelable(false);
        buttonPay = (Button) view.findViewById(R.id.buttonPay);
        editTextAmount = (EditText) view.findViewById(R.id.editTextAmount);
        editTextAmount.setText("" + amount);


        buttonPay.setOnClickListener(this);

        Intent intent = new Intent(getActivity(), PayPalService.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);
        return view;
    }

    @Override
    public void onDestroy() {
        getActivity().stopService(new Intent(getActivity(), PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        getPayment();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.e("paymentExample", paymentDetails);

                        //Starting a new activity for the payment details and also putting the payment details with intent
                        startActivity(new Intent(getActivity(), ConfirmationActivity.class)
                                .putExtra("PaymentDetails", paymentDetails)
                                .putExtra("PaymentAmount", paymentAmount));
                        */
/*JSONObject jsonDetails = new JSONObject(paymentDetails);
                        loadPoint(jsonDetails);*//*


                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private void getPayment() {
        //Getting the amount from editText
        paymentAmount = editTextAmount.getText().toString();

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "USD", "Easy Save Payment",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(getActivity(), PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    */
/*private void loadPoint(JSONObject jsonDetails) {
        pd.show();
        JSONObject params = new JSONObject();
        try {
           JSONObject jsonObjectPayment = jsonDetails.getJSONObject("response");

            params.put("key", AppConstant.KEY);
            params.put("cardNo", sessionManager.getKeyCardNumber());
            params.put("loadAmount", sessionManager.getKeyLoadAmount());
            params.put("fee", sessionManager.getKeyFee());
            params.put("amountCollected", sessionManager.getKeyTotalAmount());
            params.put("State", jsonObjectPayment.getString("state"));
            params.put("paypalTransactionId", jsonObjectPayment.getString("id"));
            params.put("reasonOfFailure", "");
            Log.e("params", params + "");


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JsonPaymentException",e+"");
            Toast.makeText(getContext(), "JSONException " + e, Toast.LENGTH_SHORT).show();

        }

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, AppConstant.LOAD_POINT, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    Log.e("loadResponseInPaypal", response + "");

                    try {
                        JSONObject json = new JSONObject(String.valueOf(response));

                        JSONObject jsonObjectHeader = json.getJSONObject("header");
                        String errorCode = jsonObjectHeader.optString("ErrorCode");
                        String errorMessage = jsonObjectHeader.optString("ErrorMessage");
                        //Toast.makeText(ConfirmationActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                        if (errorCode.equals("0")) {
                            JSONObject jsonObject = json.getJSONObject("response");
                            JSONObject objMyPoints = jsonObject.getJSONObject("myPoints");
                            String totalCash = objMyPoints.getString("totalCash");
                            String totalPoints = objMyPoints.getString("totalPoints");

                            sessionManager.savePoints(totalCash, totalPoints);
                            startActivity(new Intent(getActivity(), ConfirmationActivity.class));

                            //sessionManager.savetransferPoint(totalCash, totalPoints);
                            pd.dismiss();


                        } else {
                            Toast.makeText(getContext(), "" + errorMessage, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getContext(), NavigationDrawerActivity.class);
                            startActivity(intent);
                            pd.dismiss();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        pd.dismiss();
                        Log.e("loadResponseException", e + "");
                        Toast.makeText(getContext(), "JSONException" + e, Toast.LENGTH_LONG).show();

                    }

                } else {
                    pd.dismiss();
                    Toast.makeText(getContext(), "Response" + response, Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pd.dismiss();
                Toast.makeText(getContext(), "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        })


        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");

                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);
    }*//*



}
*/
