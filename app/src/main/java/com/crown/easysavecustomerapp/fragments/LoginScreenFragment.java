package com.crown.easysavecustomerapp.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.NavigationDrawerActivity;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-10 on 7/3/18.
 */

public class LoginScreenFragment extends Fragment {
    EditText etMobileNumber, etPassword;
    TextView tvForgetPw;
    ImageView eyeToggleVisible, eyeToggleInvisible, imgCross;
    Button btnLogin;
    String mobileNo, password;
    CheckBox checkBox;
    ProgressDialog pd;
    String mobile;
    SessionManager sessionManager;
    View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.new_login_screen_layout, container, false);
        findWidgets();
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading..");
        pd.setCancelable(false);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();
            }
        });

        tvForgetPw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
                ab.setTitle("Forget Password");
                ab.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getForgetPw();


                    }
                });

                ab.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                ab.show();

            }
        });

        eyeToggleVisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                eyeToggleVisible.setVisibility(View.GONE);
                eyeToggleInvisible.setVisibility(View.VISIBLE);

            }
        });


        eyeToggleInvisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                eyeToggleInvisible.setVisibility(View.GONE);
                eyeToggleVisible.setVisibility(View.VISIBLE);
            }
        });

        return rootView;
    }

    private void findWidgets() {
        etMobileNumber = (EditText) rootView.findViewById(R.id.et_mobileNumber);
        //etMobileNumber.setText("" + "8982951915");
        etPassword = (EditText) rootView.findViewById(R.id.et_password);
        //etPassword.setText("" + "123456");
        checkBox = (CheckBox) rootView.findViewById(R.id.checkbox);

        imgCross = (ImageView) rootView.findViewById(R.id.img_cross);
        eyeToggleVisible = (ImageView) rootView.findViewById(R.id.eyeTogglevisible);
        eyeToggleInvisible = (ImageView) rootView.findViewById(R.id.eyeToggleInvisible);
        tvForgetPw = (TextView) rootView.findViewById(R.id.tv_forget_pw);

        btnLogin = (Button) rootView.findViewById(R.id.btnn_login);
    }


    private void userLogin() {

        mobileNo = etMobileNumber.getText().toString();
        password = etPassword.getText().toString();


        if (TextUtils.isEmpty(mobileNo)) {
            etMobileNumber.setError("Please enter your Mobile Number");
            etMobileNumber.requestFocus();
            return;

        } else if (TextUtils.isEmpty(password)) {
            etPassword.setError("Please enter your Password");
            etPassword.requestFocus();
            return;
        } else if (!checkBox.isChecked()) {
            Toast.makeText(getActivity(), "Please click Terms and Condition", Toast.LENGTH_SHORT).show();

            //checkBox.requestFocus();

        } else if (!mobileNo.isEmpty() && !password.isEmpty() && checkBox.isChecked()) {
            fetchData();

        }
    }


    private void fetchData() {
        pd.show();

        String url =  AppConstant.LOGIN_URL;
        String key = AppConstant.KEY;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        JSONObject params = new JSONObject();
        try {
            params.put("key", key);
            params.put("mobileNo", etMobileNumber.getText().toString());
            params.put("password", etPassword.getText().toString());
            params.put("deviceId", sessionManager.getKeyDeviceId());
            params.put("deviceType", AppConstant.DEVICE_TYPE);

            Log.e("paramsILogin",params + "");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(getActivity(), ""+response, Toast.LENGTH_SHORT).show();

                Log.e("responseee", response + " ");
                try {

                    JSONObject obj = new JSONObject(String.valueOf(response));
                    JSONObject jsonObjectHeader = obj.getJSONObject("header");
                    String errorCode = jsonObjectHeader.optString("ErrorCode");
                    String errorMessage = jsonObjectHeader.optString("ErrorMessage");

                    if (errorCode.equals("0")) {
                        JSONObject jsonObject = obj.getJSONObject("response");
                        String CustomerID = jsonObject.optString("Customer_ID");
                        String cardNo = jsonObject.optString("cardNo");
                        String dob = jsonObject.optString("dob");
                        String email = jsonObject.optString("email");
                        String firstName = jsonObject.optString("firstName");
                        String lastName = jsonObject.optString("lastName");
                        String mobile = jsonObject.optString("mobile");
                        sessionManager.savePassword(password);
                        sessionManager.createLoginSession(CustomerID, firstName, lastName, dob, jsonObject.optString("cardNo"), email, mobileNo);
                        getPointAndMoney();


                    } else {
                        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                        pd.dismiss();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }


        };

        requestQueue.add(jsonObjectRequest);

    }

    private void getPointAndMoney() {
        String url =  AppConstant.GET_POINT;
        String key = AppConstant.KEY;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        JSONObject params = new JSONObject();
        try {
            params.put("key", key);
            params.put("cardno", sessionManager.getKeyCardNumber());
            Log.e("pointparams",params+"");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String errorMessage = null;
                try {
                    Log.e("responsePoint",response+"");

                    JSONObject objResponse = new JSONObject(String.valueOf(response));
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    errorMessage = objHeader.getString("ErrorMessage");
                    if (errorCode.equals("0")) {
                        JSONObject jsonObjectResponse = objResponse.getJSONObject("response");
                        JSONObject objMyPoints = jsonObjectResponse.getJSONObject("myPoints");
                        String totalCash = objMyPoints.getString("totalCash");
                        String totalPoints = objMyPoints.getString("totalPoints");
                        sessionManager.savePoints(totalCash, totalPoints);
                        pd.dismiss();
                        Intent ob = new Intent(getActivity(), NavigationDrawerActivity.class);
                        startActivity(ob);
                        getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), errorCode + errorMessage, Toast.LENGTH_LONG).show();
                        pd.dismiss();
                    }


                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Exception" + e, Toast.LENGTH_LONG).show();

                    pd.dismiss();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                error.printStackTrace();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        requestQueue.add(jsonObjectRequest);

    }

    private void getForgetPw() {

        // http://dev.easysavecustomer.com/EasySaveCustomerAPI.svc/ForgotPassword

        String url =  AppConstant.FORGET_PW;
        String key = AppConstant.KEY;
        final RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        JSONObject params = new JSONObject();
        try {
            params.put("key", key);
            params.put("userName", sessionManager.getKeyMobile());
            Log.e("params", params + "");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("responseFORGETPW", response + "");
                try {

                    JSONObject objResponse = new JSONObject(String.valueOf(response));
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errorMessage = objHeader.getString("ErrorMessage");
                    if (errorCode.equalsIgnoreCase("0")) {
                        Toast.makeText(getActivity(), "" + errorMessage, Toast.LENGTH_LONG).show();

                    } else if (!errorCode.equalsIgnoreCase("0")) {
                        Toast.makeText(getActivity(), "" + errorMessage, Toast.LENGTH_LONG).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();


                error.printStackTrace();
            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }


        };

        requestQueue.add(jsonObjectRequest);


    }


}