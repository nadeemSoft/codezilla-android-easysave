package com.crown.easysavecustomerapp.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.MyBookedTicketAdapter;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.models.BookedTicketModel;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyTicketFragment extends android.support.v4.app.Fragment {
    RecyclerView rvMyTickets;
    ProgressDialog pd;
    ArrayList<BookedTicketModel> myTicketList = null;
    MyBookedTicketAdapter myBookedTicketAdapter;
    SessionManager sessionManager;
    TextView tvNoRecord;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_tickets_fragment, container, false);
        rvMyTickets = (RecyclerView)view.findViewById(R.id.rv_my_tickets);
        tvNoRecord = (TextView)view.findViewById(R.id.tv_no_record);
        sessionManager = new SessionManager(getActivity());
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading..");
        myTicketList = new ArrayList();
        getMyBookedTickets();

        return view;
    }
    private void getMyBookedTickets(){
        pd.show();

       // String url = "http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/bookedtickets?key=EASYSAVECUSTOMERAPIV1-10001&cardNo=728302770208";
        String url = AppConstant.BASE_URL_GET+"bookedtickets?key=" + AppConstant.KEY + "&cardNo=" +sessionManager.getKeyCardNumber();
        Log.e("BookedTicketURL", url);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("responseBookedTicket", response + "");
                myTicketList.clear();

                try {

                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");

                    if(errorCode.equals("0")){
                        tvNoRecord.setVisibility(View.GONE);
                        rvMyTickets.setVisibility(View.VISIBLE);
                        JSONArray objReciev = objResponse.getJSONArray("tickets");
                        Log.e("myTickestResponse", objReciev + "");

                        for (int i = 0; i < objReciev.length(); i++) {
                            JSONObject objReceivedata = objReciev.getJSONObject(i);
                            String BookedDate = objReceivedata.getString("BookedDate");
                            String BookedTimeSlot = objReceivedata.getString("BookedTimeSlot");
                            String Category = objReceivedata.getString("Category");
                            String Details = objReceivedata.getString("Details");
                            String ImageUrl = objReceivedata.getString("ImageUrl");
                            String Name = objReceivedata.getString("Name");
                            String NoOfTickets = objReceivedata.getString("NoOfTickets");
                            String TicketId = objReceivedata.getString("TicketId");

                            BookedTicketModel bookedTicketModel = new BookedTicketModel(BookedDate,BookedTimeSlot,Category,Details,ImageUrl,Name,NoOfTickets,TicketId);
                            myTicketList.add(bookedTicketModel);

                        }

                        myBookedTicketAdapter = new MyBookedTicketAdapter(getActivity(), myTicketList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        rvMyTickets.setLayoutManager(linearLayoutManager);
                        rvMyTickets.setAdapter(myBookedTicketAdapter);
                        myBookedTicketAdapter.notifyDataSetChanged();

                        pd.dismiss();


                    }else {
                        pd.dismiss();
                        tvNoRecord.setVisibility(View.VISIBLE);
                        rvMyTickets.setVisibility(View.GONE);
                       // Toast.makeText(getActivity(),""+errormessage,Toast.LENGTH_LONG).show();

                    }



                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(getActivity(),"Exception"+e,Toast.LENGTH_LONG).show();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(getActivity(),"VolleyError"+error,Toast.LENGTH_LONG).show();

            }
        }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }
}
