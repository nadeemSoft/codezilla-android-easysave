package com.crown.easysavecustomerapp.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.NavigationDrawerActivity;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class SignUpScreenFragment extends Fragment {
    String cardNo, androidId;
    Button btnSignUp;
    ImageView imageCross;
    TextView tvTermAndCondition;
    EditText etFirstName, etLastName, etEmailId, etMobileNumber, etPassword, etReEnterPassword, etCountry;
    String firstName, lastName, emailId, mobileNumber, password, reEnterPassword, country;
    Spinner spinnerCountry;
    String countryList[] = {"Barbados", "Jamaica", "Trinidad", "Grenada", "St. Vincent", "Dominica", "Dominican Republic"};
    View rootView;
    ArrayAdapter<String> adapter = null;
    CheckBox checkBox;
    String countySpinner;
    ProgressDialog pd;
    SessionManager sessionManager;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    TextInputEditText textInputEditText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.new_sign_up_layout, container, false);
        androidId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        sessionManager.saveDeviceId(androidId);

        findWidgets();
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Signing up");
        pd.setCancelable(false);
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, countryList);
        spinnerCountry.setAdapter(adapter);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSignUp();


            }
        });
        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countySpinner = spinnerCountry.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return rootView;
    }

    private void findWidgets() {

        etFirstName = (EditText) rootView.findViewById(R.id.et_firstName);
        etLastName = (EditText) rootView.findViewById(R.id.et_lastName);
        etEmailId = (EditText) rootView.findViewById(R.id.et_emailId);
        etMobileNumber = (EditText) rootView.findViewById(R.id.et_mobileNo);
        etPassword = (EditText) rootView.findViewById(R.id.et_password);
        etReEnterPassword = (EditText) rootView.findViewById(R.id.et_reEnterPassword);
        spinnerCountry = (Spinner) rootView.findViewById(R.id.spinner1);
        checkBox = (CheckBox) rootView.findViewById(R.id.checkbox);
        btnSignUp = (Button) rootView.findViewById(R.id.btn_signUp);
        imageCross = (ImageView) rootView.findViewById(R.id.img_cross);
        tvTermAndCondition = (TextView) rootView.findViewById(R.id.termNcondition);
    }


    private void userSignUp() {
        firstName = etFirstName.getText().toString();
        lastName = etLastName.getText().toString();
        emailId = etEmailId.getText().toString().trim();
        mobileNumber = etMobileNumber.getText().toString();
        password = etPassword.getText().toString();
        reEnterPassword = etReEnterPassword.getText().toString();
        country = countySpinner;


//                        ===========================FIRST NAME============================
        if (TextUtils.isEmpty(firstName)) {
            etFirstName.setError("This Field can't be empty");
            etFirstName.requestFocus();
            return;
        }

//                        ===========================LAST NAME============================
        if (TextUtils.isEmpty(lastName)) {
            etLastName.setError("This Field can't be empty");
            etLastName.requestFocus();
            return;
        }


//                        ===========================EMAIL ADDRESS============================
        if (TextUtils.isEmpty(emailId)) {
            etEmailId.setError("Please Enter Your Email Id");
            etEmailId.requestFocus();
            return;
        }



//                        ===========================PASSWORD============================
        if (TextUtils.isEmpty(password)) {
            etPassword.setError("Please Enter Password");
            etPassword.requestFocus();
            return;
        }


//                        ===========================RE-ENTER PASSWORD============================
        if (TextUtils.isEmpty(reEnterPassword)) {
            etReEnterPassword.setError("Please Re-Enter Password");
            etReEnterPassword.requestFocus();
            return;
        } else {
            if (!(etReEnterPassword.getText().toString().equals(etPassword.getText().toString()))) {
                etReEnterPassword.setError("Passwords are not equal");
                etReEnterPassword.requestFocus();
                return;
            }
        }

//                        ===========================MOBILE NUMBER============================
        if (TextUtils.isEmpty(mobileNumber)) {
            etMobileNumber.setError("Enter Mobile Number");
            etMobileNumber.requestFocus();
            return;
        } else {
            if (etMobileNumber.getText().toString().length() < 10) {
                etMobileNumber.setError("Please Enter Valid Mobile Number");
                Toast.makeText(getActivity(), "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();

                return;
            }
        }

        if (!checkBox.isChecked()) {
            Toast.makeText(getActivity(), "Please click Terms and Condition", Toast.LENGTH_SHORT).show();


        }

        if (!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName) && emailId.matches(emailPattern) && !TextUtils.isEmpty(mobileNumber) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(reEnterPassword) && checkBox.isChecked()) {
            fetchData();
            btnSignUp.setText("Singing Up");
           // LoginSignUpActivity.tabLayout.getTabAt(1).setText("Singing up");

        }else if(!emailId.matches(emailPattern)){
            Toast.makeText(getActivity(), "Please Enter Valid EmailId", Toast.LENGTH_SHORT).show();

        }
    }


    private void fetchData() {
        pd.show();

        String uri = AppConstant.SIGN_UP_URL;
        String key = AppConstant.KEY;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        JSONObject param = new JSONObject();
        try {
            param.put("key", key);
            param.put("firstName", firstName);
            param.put("lastName", lastName);
            param.put("email", emailId);
            param.put("password", password);
            param.put("mobile", mobileNumber);
            param.put("Country", countySpinner);
            param.put("deviceId", sessionManager.getKeyDeviceId());
            param.put("deviceType", AppConstant.DEVICE_TYPE);

            Log.e("paramSignUp", param + "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, uri, param, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(getActivity(), "data" + response, Toast.LENGTH_SHORT).show();

                try {
                    JSONObject json = new JSONObject(String.valueOf(response));

                    JSONObject jsonObjectHeader = json.getJSONObject("header");
                    String errorCode = jsonObjectHeader.optString("ErrorCode");
                    String errorMessage = jsonObjectHeader.optString("ErrorMessage");

                    if (errorCode.equals("0")) {
                        JSONObject jsonObject = json.getJSONObject("response");

                        Log.e("responseSignUp", response + "");

                        String CustomerID = jsonObject.optString("Customer_ID");
                        cardNo = jsonObject.optString("cardNo");
                        String country = jsonObject.optString("country");
                        String email = jsonObject.optString("email");
                        String firstName = jsonObject.optString("firstName");
                        String lastName = jsonObject.optString("lastName");
                        String mobile = jsonObject.optString("mobile");
                        sessionManager.saveCountry(country);
                        sessionManager.savePassword(password);
                        sessionManager.createLoginSession(CustomerID, firstName, lastName, "dob", jsonObject.optString("cardNo"), email, mobile);

                        /*Intent intent = new Intent(getActivity(), LoginSignUpActivity.class);
                        startActivity(intent);*/
                        pd.dismiss();
                        //Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();


                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();

                        // Setting Dialog Title
                        alertDialog.setTitle("Your Registration");


                        // Setting Dialog Message
                        alertDialog.setMessage("Welcome! Your EasySave account is active.");
                        alertDialog.setCancelable(false);

                        // Setting Icon to Dialog
                        //alertDialog.setIcon(R.drawable.tick);

                        // Setting OK Button
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog closed
                                Intent intent = new Intent(getActivity(), NavigationDrawerActivity.class);
                                startActivity(intent);
                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
                        //getActivity().finish();
                    } else {
                        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                        pd.dismiss();

                    }


                } catch (JSONException e) {
                    pd.dismiss();
                    Toast.makeText(getActivity(), "JSONException" + e, Toast.LENGTH_LONG).show();


                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(getActivity(), "VolleyError" + error, Toast.LENGTH_LONG).show();

                error.printStackTrace();
                // Toast.makeText(SignUpScreenActivity.this,"error"+error,Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }


}
