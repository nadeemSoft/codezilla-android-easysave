package com.crown.easysavecustomerapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.SendReceiveHistoryActivity;
import com.crown.easysavecustomerapp.adapter.ReceiverHistoryAdapter;
import com.crown.easysavecustomerapp.adapter.RecieverHistoryAdapter;

/**
 * Created by codezilla-10 on 23/2/18.
 */

public class ReceiveHistoryFragment extends Fragment {
    RecyclerView rvLoading;
    RecieverHistoryAdapter recieverHistoryAdapter;
    LinearLayout linearNoHistory;
    ReceiverHistoryAdapter receiverHistoryAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.receive_history_layout, container, false);
        rvLoading = (RecyclerView)view.findViewById(R.id.rv_loading);
        linearNoHistory = (LinearLayout)view.findViewById(R.id.linear_no_history);
        if(SendReceiveHistoryActivity.jsonObjDatReceive.length() != 0){
           /* recieverHistoryAdapter = new RecieverHistoryAdapter(getContext(), SendReceiveHistoryActivity.receiveHistoriesList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvLoading.setLayoutManager(linearLayoutManager);
            rvLoading.setAdapter(recieverHistoryAdapter);*/

            receiverHistoryAdapter = new ReceiverHistoryAdapter(getContext(),SendReceiveHistoryActivity.jsonObjDatReceive);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvLoading.setLayoutManager(linearLayoutManager);
            rvLoading.setAdapter(receiverHistoryAdapter);
            receiverHistoryAdapter.notifyDataSetChanged();

        }else {
            rvLoading.setVisibility(View.GONE);
            linearNoHistory.setVisibility(View.VISIBLE);

        }
        return view;

    }
}
