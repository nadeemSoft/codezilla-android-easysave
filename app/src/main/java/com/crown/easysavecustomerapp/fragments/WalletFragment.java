package com.crown.easysavecustomerapp.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.activities.LoadMoneyActivity;
import com.crown.easysavecustomerapp.activities.NavigationDrawerActivity;
import com.crown.easysavecustomerapp.activities.SendMoneyActivity;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-121 on 21/3/18.
 */

public class WalletFragment extends Fragment implements View.OnClickListener {
    TextView tvAvailBalance, tvLoadMoney;
    Button btnSendMoney, btnLoadMoney;
    SessionManager sessionManager;
    ProgressDialog pd;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallet, container, false);
        getActivity().setTitle("Wallet");
        initWidgets(view);
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPointAndMoney();
        setBottomColore();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    HomeFragment homeFragment = new HomeFragment();
                    android.support.v4.app.FragmentTransaction transaction = ((NavigationDrawerActivity) getContext()).getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, homeFragment); // fragmen container id in first parameter is the  container(Main layout id) of Activity
                    transaction.commit();

                    return true;
                }
                return false;
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_money:
                Intent intentSend = new Intent(getActivity(), SendMoneyActivity.class);
                startActivity(intentSend);
                break;

            case R.id.btn_load_money:
                Intent intentLoad = new Intent(getActivity(), LoadMoneyActivity.class);
                startActivity(intentLoad);
                break;


        }

    }

    private void setData() {

        String totalCash = sessionManager.getKeyTotalCash();
        String totalPoints = sessionManager.getKeyTotalPoint();

        tvAvailBalance.setText("$" + totalCash);
        tvLoadMoney.setText("" + totalPoints);
    }

    private void initWidgets(View view) {
        tvAvailBalance = (TextView) view.findViewById(R.id.tv_available_bal);
        tvLoadMoney = (TextView) view.findViewById(R.id.tv_point_money);
        btnLoadMoney = (Button) view.findViewById(R.id.btn_load_money);
        btnLoadMoney.setEnabled(true);
        btnSendMoney = (Button) view.findViewById(R.id.btn_send_money);

        btnSendMoney.setOnClickListener(this);
        btnLoadMoney.setOnClickListener(this);


    }

    private void setBottomColore() {

        NavigationDrawerActivity.ivHome.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab1_inactive));
        NavigationDrawerActivity.tvHome.setTextColor(getResources().getColor(R.color.dark_grey));

        NavigationDrawerActivity.ivWallet.setImageDrawable(getResources().getDrawable(R.drawable.ic_walletactive));
        NavigationDrawerActivity.tvWallet.setTextColor(getResources().getColor(R.color.active_Bottom));

        NavigationDrawerActivity.ivProfile.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab3_inactive));
        NavigationDrawerActivity.tvProfile.setTextColor(getResources().getColor(R.color.dark_grey));

        NavigationDrawerActivity.ivNotification.setImageDrawable(getResources().getDrawable(R.drawable.bottomtab4_inactive));
        NavigationDrawerActivity.tvNotification.setTextColor(getResources().getColor(R.color.dark_grey));
    }

    private void getPointAndMoney() {
        pd.show();
        String url =  AppConstant.GET_POINT;
        String key = AppConstant.KEY;
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        JSONObject params = new JSONObject();
        try {
            params.put("key", key);
            params.put("cardno", sessionManager.getKeyCardNumber());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("response",String.valueOf(response)+"");

                try {

                    JSONObject objResponce = new JSONObject(String.valueOf(response));
                    JSONObject objHeader = objResponce.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errorMessage = objHeader.getString("ErrorMessage");
                    if(errorCode.equalsIgnoreCase("0")){
                        JSONObject jsonObjectResponse = objResponce.getJSONObject("response");

                        JSONObject objMyPoints = jsonObjectResponse.getJSONObject("myPoints");
                        String totalCash = objMyPoints.getString("totalCash");
                        String totalPoints = objMyPoints.getString("totalPoints");
                        sessionManager.savePoints(totalCash, totalPoints);
                        setData();
                        pd.dismiss();

                    }else if(errorCode.equalsIgnoreCase("1005")){
                        pd.dismiss();
                        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();

                    }

                    //sessionManager.savetransferPoint(totalCash, totalPoints);
                    pd.dismiss();



                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception",e+"");
                    Toast.makeText(getActivity(), "Exception"+ e, Toast.LENGTH_LONG).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                error.printStackTrace();
                Toast.makeText(getActivity(), "VolleyError"+ error, Toast.LENGTH_LONG).show();

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        requestQueue.add(jsonObjectRequest);

    }
}
