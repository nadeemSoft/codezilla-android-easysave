package com.crown.easysavecustomerapp.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.BuyTicketsAdapter;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.models.TicketsModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

public class BuyTicketsFragment extends android.support.v4.app.Fragment {
    RecyclerView rvBuyTickets;
    ProgressDialog pd;
   // public static ArrayList<TicketsModel> buyTicketsModelList = null;
    BuyTicketsAdapter buyTicketsAdapter;
    TextView tvNoRecord;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.buy_tickets_fragment, container, false);
        rvBuyTickets = (RecyclerView)view.findViewById(R.id.rv_buy_tickets);
        tvNoRecord = (TextView)view.findViewById(R.id.tv_no_record);

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading..");
        //buyTicketsModelList = new ArrayList<TicketsModel>();
       // getTickets();
        if(HomeFragment.buyTicketsModelList.size() != 0){
            tvNoRecord.setVisibility(View.GONE);
            rvBuyTickets.setVisibility(View.VISIBLE);
            buyTicketsAdapter = new BuyTicketsAdapter(getContext(),R.layout.buy_tickets_adapter, HomeFragment.buyTicketsModelList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvBuyTickets.setLayoutManager(linearLayoutManager);
            rvBuyTickets.setAdapter(buyTicketsAdapter);

        }else if(HomeFragment.buyTicketsModelList.size() == 0) {
            pd.dismiss();
            tvNoRecord.setVisibility(View.VISIBLE);
            rvBuyTickets.setVisibility(View.GONE);
        }

        return view;
    }

    private void getTickets(){
        pd.show();

        //String url = "http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/tickets?key=EASYSAVECUSTOMERAPIV1-10001";
        String url = AppConstant.BASE_URL_GET+"tickets?key=" + AppConstant.KEY ;
        Log.e("chekurl", url);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response + "");
               // buyTicketsModelList.clear();
                Arrays arrays[];
                try {

                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");

                    if(errorCode.equals("0")){
                        JSONArray objReciev = objResponse.getJSONArray("tickets");
                        Log.e("ticketsResponse", objReciev + "");

                        for (int i = 0; i < objReciev.length(); i++) {
                            JSONObject objReceivedata = objReciev.getJSONObject(i);
                            String Category = objReceivedata.getString("Category");
                            String Details = objReceivedata.getString("Details");
                            String EndDate = objReceivedata.getString("EndDate");
                            String ImageUrl = objReceivedata.getString("ImageUrl");
                            String Name = objReceivedata.getString("Name");
                            String Price = objReceivedata.getString("Price");
                            String TicketId = objReceivedata.getString("TicketId");
                            String TicketsAvaliable = objReceivedata.getString("TicketsAvaliable");
                            JSONArray TimeSlotsJsonArray = objReceivedata.getJSONArray("TimeSlots");


                            TicketsModel ticketsModel = new TicketsModel(Category,Details,EndDate,ImageUrl,Name,Price,TicketId,TicketsAvaliable,TimeSlotsJsonArray);
                           //  buyTicketsModelList.add(ticketsModel);
                        }

                     //   buyTicketsAdapter = new BuyTicketsAdapter(getContext(), buyTicketsModelList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        rvBuyTickets.setLayoutManager(linearLayoutManager);
                        rvBuyTickets.setAdapter(buyTicketsAdapter);

                        pd.dismiss();


                    }else {
                        pd.dismiss();
                        Toast.makeText(getActivity(),errormessage,Toast.LENGTH_SHORT).show();

                    }



                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(getActivity(),"Exception"+e,Toast.LENGTH_SHORT).show();



                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(getActivity(),"VolleyError"+error,Toast.LENGTH_SHORT).show();


            }
        }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }
}
