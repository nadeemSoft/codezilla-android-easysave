package com.crown.easysavecustomerapp.utils;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;

public class Share {

    String test = "https://play.google.com/store/apps/details?id=com.crown.easysavecustomerapp&hl=en";

    public void shareLink(Context context) {

        SpannableStringBuilder sb = new SpannableStringBuilder("CheckOut EasySave App - " + test);

        StyleSpan b = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold

        String sb1 = sb.toString();
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "EasySave");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, sb1);
        context.startActivity(Intent.createChooser(sharingIntent, "EasySave"));
    }
}
