package com.crown.easysavecustomerapp.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.crown.easysavecustomerapp.activities.LoginSignUpActivity;

import java.util.HashMap;

public class SessionManager {
    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "AndroidHivePref";

    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String KEY_DEVICE_ID = "deviceId";


    public static final String KEY_ID = "id";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_DOB = "dob";
    public static final String KEY_CARD_NUMBER = "card_number";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_PW = "password";

    public static final String KEY_COUNTRY = "country";


    public static final String KEY_TOTAL_CASH = "total_cash";
    public static final String KEY_TOTAL_POINT = "total_point";

    public static final String KEY_TRANSFER_TOTAL_CASH = "transfer_total_cash";
    public static final String KEY_TRANSFER_TOTAL_POINT = "transfer_total_point";

    public static final String KEY_TOTAL_AMOUNT = "total_amount";
    public static final String KEY_FEE = "fee";
    public static final String KEY_LOAD_AMOUNT = "load_amount";
    public static final String KEY_ACTUAL_AMOUNT = "actual_amount";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(String id, String f_name, String l_name, String dob, String card_number, String email, String mobile) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_ID, id);
        editor.putString(KEY_FIRST_NAME, f_name);
        editor.putString(KEY_LAST_NAME, l_name);
        editor.putString(KEY_DOB, dob);
        editor.putString(KEY_CARD_NUMBER, card_number);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.commit();
    }

    public void saveUpdatePrifile(String id, String f_name, String l_name, String email, String mobile) {
        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_ID, id);
        editor.putString(KEY_FIRST_NAME, f_name);
        editor.putString(KEY_LAST_NAME, l_name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.commit();
    }

    public void savePoints(String totalCash, String totalPoint) {

        editor.putString(KEY_TOTAL_CASH, totalCash);
        editor.putString(KEY_TOTAL_POINT, totalPoint);
        editor.commit();
    }
    public void savetransferPoint(String totalTransferCash, String totalTransferPoint) {

        editor.putString(KEY_TOTAL_CASH, totalTransferCash);
        editor.putString(KEY_TOTAL_POINT, totalTransferPoint);
        editor.commit();
    }

    public void saveCountry(String country) {

        editor.putString(KEY_COUNTRY, country);
        editor.commit();
    }

    public void saveDeviceId(String deviceId) {

        editor.putString(KEY_DEVICE_ID, deviceId);
        editor.commit();
    }


    public void savePassword(String password) {

        editor.putString(KEY_PW, password);
        editor.commit();
    }

    public void saveActualAmount(String amount) {
        editor.putString(KEY_ACTUAL_AMOUNT, amount);
        editor.commit();
    }

    public void saveAmount(int totalAmount, int fee, int loadAmount) {

        editor.putInt(KEY_TOTAL_AMOUNT, totalAmount);
        editor.putInt(KEY_FEE, fee);
        editor.putInt(KEY_LOAD_AMOUNT, loadAmount);
        editor.commit();
    }

    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginSignUpActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }


    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_FIRST_NAME, pref.getString(KEY_FIRST_NAME, null));

        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginSignUpActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getKeyMobile() {
        return pref.getString(KEY_MOBILE, "");
    }

    public String getKeyId() {
        return pref.getString(KEY_ID, "");
    }

    public String getKeyCardNumber() {
        return pref.getString(KEY_CARD_NUMBER, "");
    }

    public String getKeyTotalCash() {
        return pref.getString(KEY_TOTAL_CASH, "");
    }

    public String getKeyTotalPoint() {
        return pref.getString(KEY_TOTAL_POINT, "");
    }

    public String getKeyEmail() {
        return pref.getString(KEY_EMAIL, "");
    }

    public String getKeyDeviceId() {
        return pref.getString(KEY_DEVICE_ID, "");
    }


    public int getKeyTotalAmount() {
        return pref.getInt(KEY_TOTAL_AMOUNT, 0);
    }

    public int getKeyFee() {
        return pref.getInt(KEY_FEE, 0);
    }
    public String getActualAmount() {
        return pref.getString(KEY_ACTUAL_AMOUNT, "");
    }

    public int getKeyLoadAmount() {
        return pref.getInt(KEY_LOAD_AMOUNT, 0);
    }

    public String getKeyFirstName() {
        return pref.getString(KEY_FIRST_NAME, "");
    }

    public String getKeyLastName() {
        return pref.getString(KEY_LAST_NAME, "");
    }

    public String getKeyCountry() {
        return pref.getString(KEY_COUNTRY, "");
    }

    public String getKeyPassword() {
        return pref.getString(KEY_PW, "");
    }
}