package com.crown.easysavecustomerapp.helper;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

/**
 * Created by codezilla-10 on 28/2/18.
 */

public class AppConstant {
    public static final String KEY = "EASYSAVECUSTOMERAPIV1-10001";
    public static final String DEVICE_TYPE = "a";
    public static final String BASE_URL_GET = " http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/";

    public static final String BASE_SERVICE_URL = "http://dev.easysavecustomer.com/EasySaveCustomerAPI.svc/";

    public static final String GET_PAYMENT_BASE = " http://api.easysavecustomer.com/EasySaveCustomerBill.svc/";
    public static final String PAY = GET_PAYMENT_BASE + "pay";

    public static final String CHANGE_PASSWORD = BASE_URL_GET + "changepassword";
    public static final String UPDATE_PROFILE = BASE_URL_GET + "updateeprofile";

    public static final String LOAD_POINT = BASE_URL_GET + "load";
    public static final String LOGIN_URL = BASE_URL_GET + "Login";
    public static final String SIGN_UP_URL = BASE_URL_GET + "RegisterCustomer";
    public static final String FORGET_PW = BASE_URL_GET + "ForgotPassword";
    public static final String GET_POINT = BASE_URL_GET + "MyPoints";
    public static final String CONTACT_US = BASE_URL_GET + "ContactUs";

    public static final String TRANSFER_POINT = BASE_SERVICE_URL + "transfer";
    public static final String BASE_URL_BILL = "http://api.easysavemerchants.com/EasySaveAPI.svc/";
    public static final String BILL_SERVICES = BASE_URL_BILL + "billservices?key=EASYSAVEAPIV1-10001";
    public static final String BOOK_TICKET = BASE_URL_GET + "bookticket";

    public static byte[] getFileDataFromDrawable(Context context, Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();

    }
}
