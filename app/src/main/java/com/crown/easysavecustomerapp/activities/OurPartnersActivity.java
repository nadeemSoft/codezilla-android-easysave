package com.crown.easysavecustomerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.crown.easysavecustomerapp.R;

public class OurPartnersActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_our_partners);
        this.setTitle("Our Partners");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        webView = (WebView)findViewById(R.id.web_view);
        webView.loadUrl("http://admin.easysavemerchants.com/Partner/Partners");
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(OurPartnersActivity.this,NavigationDrawerActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();

    }

}
