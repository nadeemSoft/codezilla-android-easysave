package com.crown.easysavecustomerapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.fragments.StripeFragment;

import java.util.ArrayList;
import java.util.List;


public class SelectPaymentActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager viewPager;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_payment);
        this.setTitle("Select Payment Option");

        viewPager = (ViewPager) findViewById(R.id.viewPager_select_payment);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout_select_payment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);


    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }


    private void setupViewPager(ViewPager viewPager) {

        SelectPaymentActivity.ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
       // adapter.addFragment(new CreditCardFragment(), "CREDIT CARD");
        adapter.addFragment(new StripeFragment(), "STRIPE");
        viewPager.setAdapter(adapter);
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}

