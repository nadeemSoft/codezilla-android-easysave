package com.crown.easysavecustomerapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ConfirmationActivity extends AppCompatActivity {
    ProgressDialog pd;
    SessionManager sessionManager;
    Button btnSendMoney, btnPayBill;
    TextView textViewAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        this.setTitle("Load Confirmation");
        sessionManager = new SessionManager(ConfirmationActivity.this);

        pd = new ProgressDialog(ConfirmationActivity.this);
        pd.setMessage("Loading Point...");
        pd.setCancelable(false);

        textViewAmount = (TextView) findViewById(R.id.paymentAmount);
        btnPayBill = (Button) findViewById(R.id.btn_pay_bill);
        btnSendMoney = (Button) findViewById(R.id.btn_send_money_confirmation);
        btnSendMoney.setEnabled(false);
        btnPayBill.setEnabled(false);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //Getting Intent
        Intent intent = getIntent();


        try {
            JSONObject jsonDetails = new JSONObject(intent.getStringExtra("PaymentDetails"));

            //Displaying payment details
            showDetails(jsonDetails.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
        } catch (JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

       // textViewAmount.setText("$" + sessionManager.getActualAmount());

        btnPayBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSend = new Intent(ConfirmationActivity.this, PaySeeAllActivity.class);
                startActivity(intentSend);
                finish();
            }
        });

        btnSendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSend = new Intent(ConfirmationActivity.this, SendMoneyActivity.class);
                startActivity(intentSend);
                finish();

            }
        });
    }

    private void showDetails(JSONObject jsonDetails, String paymentAmount) throws JSONException {
        //Views
        //TextView textViewId = (TextView) findViewById(R.id.paymentId);
        //TextView textViewStatus = (TextView) findViewById(R.id.paymentStatus);
         loadPoint(jsonDetails);
        textViewAmount.setText("$" + sessionManager.getActualAmount());

        //Showing the details from json object
        // textViewId.setText(jsonDetails.getString("id"));
        // textViewStatus.setText(jsonDetails.getString("state"));


    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void loadPoint(JSONObject jsonDetails) {
        pd.show();
        JSONObject params = new JSONObject();
        try {
            params.put("key", AppConstant.KEY);
            params.put("cardNo", sessionManager.getKeyCardNumber());
            params.put("loadAmount", sessionManager.getKeyLoadAmount());
            params.put("fee", sessionManager.getKeyFee());
            params.put("amountCollected", sessionManager.getKeyTotalAmount());
            params.put("State", jsonDetails.getString("state"));
            params.put("paypalTransactionId", jsonDetails.getString("id"));
            params.put("reasonOfFailure", "");
            Log.e("params", params + "");


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(ConfirmationActivity.this, "JSONException " + e, Toast.LENGTH_SHORT).show();

        }

        RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, AppConstant.LOAD_POINT, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    Log.e("loadResponseInConfirm", response + "");

                    try {
                        JSONObject json = new JSONObject(String.valueOf(response));

                        JSONObject jsonObjectHeader = json.getJSONObject("header");
                        String errorCode = jsonObjectHeader.optString("ErrorCode");
                        String errorMessage = jsonObjectHeader.optString("ErrorMessage");
                        //Toast.makeText(ConfirmationActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                        if (errorCode.equals("0")) {
                            JSONObject jsonObject = json.getJSONObject("response");
                            JSONObject objMyPoints = jsonObject.getJSONObject("myPoints");
                            String totalCash = objMyPoints.getString("totalCash");
                            String totalPoints = objMyPoints.getString("totalPoints");
                            sessionManager.savePoints(totalCash, totalPoints);
                            pd.dismiss();
                            btnSendMoney.setEnabled(true);
                            btnPayBill.setEnabled(true);

                        } else {
                            Toast.makeText(ConfirmationActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(ConfirmationActivity.this, NavigationDrawerActivity.class);
                            startActivity(intent);
                            pd.dismiss();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        pd.dismiss();
                        Toast.makeText(ConfirmationActivity.this, "JSONException" + e, Toast.LENGTH_LONG).show();

                    }

                } else {
                    pd.dismiss();
                    Toast.makeText(ConfirmationActivity.this, "Response" + response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pd.dismiss();
                Toast.makeText(ConfirmationActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        })


        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");

                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);
    }
}