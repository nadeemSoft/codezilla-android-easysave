package com.crown.easysavecustomerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;

public class TransferMoneyConfirmationActivity extends AppCompatActivity implements View.OnClickListener {
    int transferMoney;
    TextView tvAmount;
    Button btnSendMoney,btnPayBill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_monet);
        this.setTitle("Transfer Confirmation");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        transferMoney = intent.getIntExtra("transferMoney", 0);
        tvAmount = (TextView) findViewById(R.id.amount);
        tvAmount.setText("$"+transferMoney);
        btnSendMoney = (Button)findViewById(R.id.btn_send_money);
        btnSendMoney.setOnClickListener(this);
        btnPayBill = (Button)findViewById(R.id.btn_pay_bill);
        btnPayBill.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {

      //  onBackPressed();
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_send_money:
                Intent intent = new Intent(TransferMoneyConfirmationActivity.this,SendMoneyActivity.class);
                startActivity(intent);
                break;

            case R.id.btn_pay_bill:
                Intent intentPay = new Intent(TransferMoneyConfirmationActivity.this,PaySeeAllActivity.class);
                startActivity(intentPay);
                break;
        }

    }
}
