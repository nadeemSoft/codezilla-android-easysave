package com.crown.easysavecustomerapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-10 on 9/3/18.
 */

public class SendMoneyActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvBalance, tvPoint;
    EditText etAmount, etName, etCardNo;
    Button btnProceedMoney;
    SessionManager sessionManager;
    ProgressDialog pd = null;
    double cash, points;
    int amount;
    String benificialCardNo = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_send_money_layout);
        sessionManager = new SessionManager(this);
        this.setTitle("Send Money");
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        intiWidgets();


    }

    @Override
    protected void onResume() {
        super.onResume();
        getPointAndMoney();

        Toast.makeText(SendMoneyActivity.this, "please Enter 12 digit Card No", Toast.LENGTH_SHORT).show();
        etCardNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                benificialCardNo = etCardNo.getText().toString();


            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("card",benificialCardNo+"");
                if(s.toString().length() == 12 ){
                    getReciever(benificialCardNo);

                }
            }
        });



    }

    @Override
    public boolean onSupportNavigateUp() {
        //onBackPressed();
        Intent intent = new Intent(SendMoneyActivity.this,NavigationDrawerActivity.class);
        startActivity(intent);
        finish();
        return true;
    }

    private void setData() {
        String totalCash = sessionManager.getKeyTotalCash();
        String totalPoints = sessionManager.getKeyTotalPoint();
        try {
            if(sessionManager.getKeyTotalCash() != null){
                cash = Double.parseDouble(sessionManager.getKeyTotalCash());

            }

        }catch (Exception e){

        }

        tvBalance.setText("$" + totalCash);
        tvPoint.setText("" + totalPoints);

    }

    private void intiWidgets() {
        tvBalance = (TextView) findViewById(R.id.tv_balance);
        tvPoint = (TextView) findViewById(R.id.tv_point_mo);
        etAmount = (EditText) findViewById(R.id.et_amount);
        etCardNo = (EditText) findViewById(R.id.et_card_no);
        etName = (EditText) findViewById(R.id.et_name);
        btnProceedMoney = (Button) findViewById(R.id.btn_save_changes);
        btnProceedMoney.setOnClickListener(this);
        btnProceedMoney.setEnabled(false);
    }

    private void getPointAndMoney() {
        pd.show();
        String url =  AppConstant.GET_POINT;
        String key = AppConstant.KEY;
        RequestQueue requestQueue = Volley.newRequestQueue(SendMoneyActivity.this);

        JSONObject params = new JSONObject();
        try {
            params.put("key", key);
            params.put("cardno", sessionManager.getKeyCardNumber());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONObject objResponce = new JSONObject(String.valueOf(response));
                    JSONObject objHeader = objResponce.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errorMessage = objHeader.getString("ErrorMessage");
                    if (errorCode.equalsIgnoreCase("0")){
                        JSONObject jsonObjectResponse = objResponce.getJSONObject("response");
                        JSONObject objMyPoints = jsonObjectResponse.getJSONObject("myPoints");
                        String totalCash = objMyPoints.getString("totalCash");
                        String totalPoints = objMyPoints.getString("totalPoints");
                        sessionManager.savePoints(totalCash, totalPoints);
                        pd.dismiss();
                        setData();
                    }else if(errorCode.equalsIgnoreCase("1005")){
                        Toast.makeText(SendMoneyActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        pd.dismiss();
                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(SendMoneyActivity.this, "Exception"+ e, Toast.LENGTH_LONG).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                error.printStackTrace();
                Toast.makeText(SendMoneyActivity.this, "VolleyError"+ error, Toast.LENGTH_LONG).show();

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        requestQueue.add(jsonObjectRequest);

    }

    private void getReciever(String benificialCardNo) {
        pd.show();
        //http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/recievername?key=EASYSAVECUSTOMERAPIV1-10001&cardNo=728302770208
        String url = AppConstant.BASE_URL_GET + "recievername?key=" + AppConstant.KEY + "&cardNo=" + benificialCardNo;
        Log.e("url", url + "");

        RequestQueue requestQueue = Volley.newRequestQueue(SendMoneyActivity.this);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("responseSendMoney", response + "");

                try {
                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");
                    //Toast.makeText(SendMoneyActivity.this, errormessage, Toast.LENGTH_SHORT).show();
                    if (errorCode.equals("0")) {
                        JSONObject objReceivedata = objResponse.getJSONObject("response");
                        etName.setText("" + objReceivedata.getString("recieverName"));
                        btnProceedMoney.setEnabled(true);
                        pd.dismiss();

                    }else {
                        Toast.makeText(SendMoneyActivity.this, "Please Enter valid Card No", Toast.LENGTH_SHORT).show();
                        pd.dismiss();

                    }

                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(SendMoneyActivity.this, "Exception " + e, Toast.LENGTH_SHORT).show();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(SendMoneyActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        });
        requestQueue.add(request);

    }


    private void loadPoint() {

        pd.show();
        JSONObject params = new JSONObject();
        try {
            params.put("key", AppConstant.KEY);
            params.put("cardNo", sessionManager.getKeyCardNumber());
            params.put("beneficiaryCardNo",benificialCardNo);
            params.put("transferAmount", amount);
            Log.e("paramsLoadPoint", params + "");


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(SendMoneyActivity.this, "JSONException " + e, Toast.LENGTH_SHORT).show();

        }

        RequestQueue requestQueue = Volley.newRequestQueue(SendMoneyActivity.this);

        String url = "http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/transfer";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ResponseSendMoney", response + "");
                pd.dismiss();

                if (response != null) {
                    try {
                        JSONObject objResponse = new JSONObject(response.toString());
                        JSONObject objHeader = objResponse.getJSONObject("header");
                        String errorCode = objHeader.getString("ErrorCode");
                        String errorMessage = objHeader.getString("ErrorMessage");
                       // Toast.makeText(SendMoneyActivity.this, errorMessage, Toast.LENGTH_LONG).show();

                        if (errorCode.equals("0")) {
                            JSONObject jsonObjectResponse = objResponse.getJSONObject("response");
                            JSONObject objMyPoints = jsonObjectResponse.getJSONObject("myPoints");
                            String transferTotalCash = objMyPoints.getString("totalCash");
                            String transferTotalPoints = objMyPoints.getString("totalPoints");
                            sessionManager.savetransferPoint(transferTotalCash, transferTotalPoints);
                           // sessionManager.savePoints(transferTotalCash, transferTotalPoints);

                            Intent intent = new Intent(SendMoneyActivity.this,TransferMoneyConfirmationActivity.class);
                            intent.putExtra("transferMoney",amount);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(SendMoneyActivity.this, errorCode + errorMessage, Toast.LENGTH_LONG).show();
                            pd.dismiss();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        pd.dismiss();
                        Toast.makeText(SendMoneyActivity.this, "JSONException" + e, Toast.LENGTH_LONG).show();

                    }

                } else {
                    pd.dismiss();
                    Toast.makeText(SendMoneyActivity.this, "Response" + response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pd.dismiss();
                Toast.makeText(SendMoneyActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save_changes:
                if (checkValidationForMoney()) {
                    loadPoint();
                }
                break;

        }

    }

    private boolean checkValidationForMoney() {
        try {
            amount = Integer.parseInt(etAmount.getText().toString());
            if (etCardNo.getText().toString().equalsIgnoreCase("") || etCardNo.getText().toString().length() != 12) {
                Toast.makeText(SendMoneyActivity.this, "Enter card No", Toast.LENGTH_SHORT).show();
                return false;
            } else if (etAmount.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(SendMoneyActivity.this, "Enter Amount", Toast.LENGTH_SHORT).show();
                return false;
            }  else if (amount <= 0 || etAmount.getText().toString().equalsIgnoreCase("0")) {
                Toast.makeText(SendMoneyActivity.this, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                return false;
            }else if (cash < amount || cash == 0 ) {
                Toast.makeText(SendMoneyActivity.this, "InSufficient Balance", Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (Exception e) {
            Toast.makeText(SendMoneyActivity.this, "Enter Amound", Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;

    }
}
