package com.crown.easysavecustomerapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-10 on 9/3/18.
 */

public class LoadMoneyActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvBalance, tvPoint;
    EditText etAmount, etLoadFre, etTotal;
    Button btnProceedMoney;
    int loadeFree = 1;
    String totalCash, totalPoints;
    int amountTotal;
    double cash, points, amountChk;
    SessionManager sessionManager;
    ProgressDialog pd;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_load_money_layout);
        this.setTitle("Load Money");
        sessionManager = new SessionManager(this);
        pd = new ProgressDialog(this);
        pd.setCancelable(false);
        intiWidgets();
        setData();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etAmount.getText().toString().length() == 0) {
                    etLoadFre.setText("");
                    etTotal.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {

                    String amount = etAmount.getText().toString();
                    sessionManager.saveActualAmount(amount);
                    int chkAmount = Integer.parseInt(amount);
                    amountChk = chkAmount;

                    int loadFreeChek = chkAmount / 50;

                    if (chkAmount % 50 == 0) {

                        loadeFree = loadFreeChek;
                    } else {

                        loadeFree = loadFreeChek + 1;
                    }
                    //Log.e("testapp",loadFreeChek+"");
                   // amountTotal = Integer.parseInt(amount) + loadeFree;
                    sessionManager.saveAmount(amountTotal, loadeFree, chkAmount);

                    if (etAmount.getText().toString().length() != 0) {
                        etLoadFre.setText("" + loadeFree);
                        //etTotal.setText(amountTotal + "");

                        etTotal.setText(amount + "");


                    }
                } catch (Exception e) {

                }


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPointAndMoney();

    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }


    private void setData() {
        totalCash = sessionManager.getKeyTotalCash();
        totalPoints = sessionManager.getKeyTotalPoint();

        tvBalance.setText("$" + totalCash);
        tvPoint.setText("" + totalPoints);
        try {
            cash = Double.parseDouble(sessionManager.getKeyTotalCash());
            points = Double.parseDouble(sessionManager.getKeyTotalPoint());

        } catch (Exception e) {
            Toast.makeText(LoadMoneyActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

        }


    }


    private void intiWidgets() {
        tvBalance = (TextView) findViewById(R.id.tv_balance_load);
        tvPoint = (TextView) findViewById(R.id.tv_point_load);
        etAmount = (EditText) findViewById(R.id.et_amount);
        etLoadFre = (EditText) findViewById(R.id.et_loadefree);
        etTotal = (EditText) findViewById(R.id.et_total);
        btnProceedMoney = (Button) findViewById(R.id.btn_proceed_money);
        btnProceedMoney.setOnClickListener(this);

    }

    private boolean checkValidationForMoney() {
        if (etAmount.getText().toString().equalsIgnoreCase("") || etAmount.getText().toString().equalsIgnoreCase("0")) {
            Toast.makeText(LoadMoneyActivity.this, "Enter Amount", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_proceed_money:
                if (checkValidationForMoney()) {
                    Intent intentProceedMoney = new Intent(LoadMoneyActivity.this, SelectPaymentActivity.class);
                    startActivity(intentProceedMoney);
                }

                break;
        }

    }
    private void getPointAndMoney() {
        pd.show();
        String url =  AppConstant.GET_POINT;
        String key = AppConstant.KEY;
        RequestQueue requestQueue = Volley.newRequestQueue(LoadMoneyActivity.this);

        JSONObject params = new JSONObject();
        try {
            params.put("key", key);
            params.put("cardno", sessionManager.getKeyCardNumber());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("pointinticket",response+"");
                    JSONObject objResponce = new JSONObject(String.valueOf(response));
                    JSONObject objHeader = objResponce.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errorMessage = objHeader.getString("ErrorMessage");
                    if (errorCode.equalsIgnoreCase("0")){
                        JSONObject jsonObjectResponse = objResponce.getJSONObject("response");
                        JSONObject objMyPoints = jsonObjectResponse.getJSONObject("myPoints");
                        String totalCash = objMyPoints.getString("totalCash");
                        String totalPoints = objMyPoints.getString("totalPoints");
                        sessionManager.savePoints(totalCash, totalPoints);
                        pd.dismiss();
                        setData();
                    }else if(errorCode.equalsIgnoreCase("1005")){
                        Toast.makeText(LoadMoneyActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        pd.dismiss();
                    }

                    //sessionManager.savetransferPoint(totalCash, totalPoints);




                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(LoadMoneyActivity.this, "Exception"+ e, Toast.LENGTH_LONG).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                error.printStackTrace();
                Toast.makeText(LoadMoneyActivity.this, "VolleyError"+ error, Toast.LENGTH_LONG).show();

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        requestQueue.add(jsonObjectRequest);

    }
}

/*
if (checkValidationForMoney()) {
        Intent intentProceedMoney = new Intent(LoadMoneyActivity.this, SelectPaymentActivity.class);
        startActivity(intentProceedMoney);
        break;

        }*/
