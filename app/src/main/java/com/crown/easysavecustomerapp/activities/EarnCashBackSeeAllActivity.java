package com.crown.easysavecustomerapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.EarnCashBackAdapter;
import com.crown.easysavecustomerapp.fragments.HomeFragment;

public class EarnCashBackSeeAllActivity extends AppCompatActivity {
    EarnCashBackAdapter earnCashBackAdapter;
    RecyclerView rvBuyTicketSeeAll;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_ticket_see_all);
        rvBuyTicketSeeAll = (RecyclerView) findViewById(R.id.rv_buy_see_all);
        this.setTitle("Home");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //setAdapter();
        earnCashBackAdapter = new EarnCashBackAdapter(this, R.layout.adapter_earn_cash_back_grid_see_all, HomeFragment.earnCashBackList);
        rvBuyTicketSeeAll.setLayoutManager(new GridLayoutManager(this, 3));
        rvBuyTicketSeeAll.setAdapter(earnCashBackAdapter);

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
