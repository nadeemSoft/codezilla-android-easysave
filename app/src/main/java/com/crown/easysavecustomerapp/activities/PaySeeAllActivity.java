package com.crown.easysavecustomerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.PayBillSeeAllAdapter;
import com.crown.easysavecustomerapp.fragments.HomeFragment;

public class PaySeeAllActivity extends AppCompatActivity {
    RecyclerView rvPaySeeAll;
    PayBillSeeAllAdapter payBillSeeAllAdapter;
    public static PaySeeAllActivity paySeeAllActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_see_all);
        paySeeAllActivity = this;
        this.setTitle("Home");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initWidgets();
        setAdapter();
    }

    @Override
    public boolean onSupportNavigateUp() {
        //onBackPressed();
        Intent intent = new Intent(PaySeeAllActivity.this,NavigationDrawerActivity.class);
        startActivity(intent);
        finish();
        return true;
    }

    private void initWidgets() {
        rvPaySeeAll = (RecyclerView) findViewById(R.id.rv_paySeeAll);

    }

    private void setAdapter() {
        payBillSeeAllAdapter = new PayBillSeeAllAdapter(getApplicationContext(), HomeFragment.billServicList);
        rvPaySeeAll.setLayoutManager(new GridLayoutManager(this, 3));
        rvPaySeeAll.setAdapter(payBillSeeAllAdapter);
    }
}
