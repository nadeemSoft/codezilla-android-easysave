package com.crown.easysavecustomerapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.BuyTicketsAdapter;
import com.crown.easysavecustomerapp.fragments.HomeFragment;

public class BuyTicketSeeAllActivity extends AppCompatActivity {
    RecyclerView rvBuyTicketSeeAll;
    BuyTicketsAdapter buyTicketsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_ticket_see_all);
        rvBuyTicketSeeAll = (RecyclerView) findViewById(R.id.rv_buy_see_all);
        this.setTitle("Home");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //setAdapter();
        buyTicketsAdapter = new BuyTicketsAdapter(this,R.layout.adapter_buy_ticket_sell_all_grid, HomeFragment.buyTicketsModelList);
        rvBuyTicketSeeAll.setLayoutManager(new GridLayoutManager(this, 3));
        rvBuyTicketSeeAll.setAdapter(buyTicketsAdapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
