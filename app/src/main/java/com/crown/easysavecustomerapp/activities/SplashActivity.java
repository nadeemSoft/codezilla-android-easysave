package com.crown.easysavecustomerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.utils.SessionManager;

/**
 * Created by codezilla-121 on 19/3/18.
 */

public class SplashActivity  extends AppCompatActivity {
    SessionManager sessionManager = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sessionManager = new SessionManager(this);
        getSupportActionBar().hide();

        try {
            if (sessionManager.isLoggedIn()) {
                Intent intent = new Intent(SplashActivity.this, NavigationDrawerActivity.class);
                startActivity(intent);
                finish();

            } else {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        // TODO: Your application init goes here.
                        Intent intent = new Intent(SplashActivity.this, ViewPagerActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 5000);
            }

        }catch (Exception e){

        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}

