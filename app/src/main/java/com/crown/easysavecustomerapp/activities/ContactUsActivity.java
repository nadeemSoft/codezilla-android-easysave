package com.crown.easysavecustomerapp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.helper.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-10 on 1/3/18.
 */

public class ContactUsActivity extends AppCompatActivity {

    EditText etSubject, etEmail, etMessage;
    Button btnSend;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us_layout);
        this.setTitle("Contact Us");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findWidgets();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = etEmail.getText().toString();
                Log.e("chkEmail",email+"");

                if (email.matches(emailPattern))
                {

                    //Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
                    fetchData();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {

        Intent intent = new Intent(ContactUsActivity.this,NavigationDrawerActivity.class);
        startActivity(intent);
        return true;
    }

    private void findWidgets() {

        etSubject = (EditText) findViewById(R.id.etSubject);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etMessage = (EditText) findViewById(R.id.etMessage);
        btnSend = (Button) findViewById(R.id.btn_send);
    }

    private void fetchData() {
        String url =  AppConstant.CONTACT_US;
        String key = AppConstant.KEY;
        RequestQueue queue = Volley.newRequestQueue(ContactUsActivity.this);


        JSONObject param = new JSONObject();
        try {
            param.put("key", key);
            param.put("email", email);
            param.put("subject", etSubject.getText().toString());
            param.put("message", etMessage.getText().toString());
            Log.e("params", param + "");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, param, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                //Toast.makeText(ContactUsActivity.this, "ghhhhhhhhhhhhhhh" + response, Toast.LENGTH_SHORT).show();
                Log.e("response of contactList", response + "");

                try {
                    JSONObject obj = new JSONObject(String.valueOf(response));

                    JSONObject jsonObjectHeader = obj.getJSONObject("header");

                    String errorCode = jsonObjectHeader.optString("ErrorCode");
                    String errorMessage = jsonObjectHeader.optString("ErrorMessage");

                    if (errorCode.equals("0")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ContactUsActivity.this);
                        builder.setMessage(errorMessage)
                                .setTitle("Contact Easysave ")
                                .setMessage("Your message has been sent successfully, we will contact you soon!")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent inHome = new Intent(ContactUsActivity.this,NavigationDrawerActivity.class);
                                        startActivity(inHome);
                                    }
                                });
                        AlertDialog dialog = builder.create();
                        dialog.show();

                      /*  String to = email;
                        String subject = "Subject:Feedback";
                        String body = "Send from my Android Phone";
                        String mailTo = "mailto:" + to +
                                "?&subject=" + Uri.encode(subject) +
                                "&body=" + Uri.encode(body);
                        Intent emailIntent = new Intent(Intent.ACTION_VIEW);
                        emailIntent.setData(Uri.parse(mailTo));
                        startActivity(emailIntent);*/
                    } else {
                        Toast.makeText(ContactUsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(ContactUsActivity.this);
                        builder.setMessage(errorMessage)
                                .setTitle("Something is going wrong")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }


}
  /*  private boolean checkValidation(){

        if (etEmail.getText().toString().equalsIgnoreCase("")){

            Toast.makeText(this, "Enter Email Address", Toast.LENGTH_SHORT).show();
            return false;
        }else if (false){

            Toast.makeText(this, "Enter Valid Email Address", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }*/