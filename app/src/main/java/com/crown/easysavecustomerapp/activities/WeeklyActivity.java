package com.crown.easysavecustomerapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.WeeklyAdapter;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.models.Offers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class WeeklyActivity extends AppCompatActivity {
    RecyclerView recyclerViewl;
    ArrayList<Offers> offerlist = null;
    ProgressDialog pd;
    WeeklyAdapter weeklyAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        this.setTitle("Weeklys");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        offerlist = new ArrayList<>();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        recyclerViewl = findViewById(R.id.rv_offers);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOffers();
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(WeeklyActivity.this,NavigationDrawerActivity.class);
        startActivity(intent);
        finish();
        return true;
    }

    private void getOffers(){
        pd.show();


        //http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/offers?key=EASYSAVECUSTOMERAPIV1-10001
        String url = AppConstant.BASE_URL_GET+"offers?key=" + AppConstant.KEY ;
        Log.e("chekurl", url);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response + "");
                offerlist.clear();
                try {

                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");

                    if(errorCode.equals("0")){
                        JSONArray objReciev = objResponse.getJSONArray("offers");
                        Log.e("OfferResponse", objReciev + "");

                        for (int i = 0; i < objReciev.length(); i++) {
                            JSONObject objReceivedata = objReciev.getJSONObject(i);
                            String Description = objReceivedata.getString("Description");
                            String Name = objReceivedata.getString("Name");
                            String OfferId = objReceivedata.getString("OfferId");
                            String OfferImage = objReceivedata.getString("OfferImage");

                            Offers offers = new Offers(OfferImage,Name,OfferId,Description);
                            offerlist.add(offers);
                        }

                        weeklyAdapter = new WeeklyAdapter(WeeklyActivity.this,R.layout.adapter_notification,offerlist);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(WeeklyActivity.this, LinearLayoutManager.VERTICAL, false);
                        recyclerViewl.setLayoutManager(linearLayoutManager);
                        recyclerViewl.setAdapter(weeklyAdapter);

                        pd.dismiss();


                    }else {
                        pd.dismiss();
                        Toast.makeText(WeeklyActivity.this,errormessage,Toast.LENGTH_SHORT).show();

                    }



                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(WeeklyActivity.this,"Exception"+e,Toast.LENGTH_SHORT).show();



                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(WeeklyActivity.this,"VolleyError"+error,Toast.LENGTH_SHORT).show();


            }
        }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(WeeklyActivity.this);
        requestQueue.add(request);
    }

    }

