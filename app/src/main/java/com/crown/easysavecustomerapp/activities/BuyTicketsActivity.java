package com.crown.easysavecustomerapp.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.fragments.HomeFragment;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.models.TicketsModel;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BuyTicketsActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvDoller, tvName, tvWork, tvDescription, tvTypeName;
    Button btnPurchase;
    ImageView imageView;
    int mYear, mMonth, mDay;
    int noOfTicket;
    double cash;
    EditText etDate, etTime, etNoOfTickets;
    TextView tvTime, tvDate;
    LinearLayout linearPlus, linearMinus;
    ArrayAdapter<String> adapter = null;
    TicketsModel ticketsModel;
    ArrayList<String> arrayList;
    SessionManager sessionManager;
    String eventDate;
    ProgressDialog pd;
    Dialog dialog;
    DatePickerDialog datePickerDialogE;
    long miliSecsDate, endDate, currentDate;
    ImageView ivPlus, ivMinus;
    Calendar ce;
    Button btnBookTicket;
    int ticketPrice;
    int totalMoney;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_tickets);
        this.setTitle("Tickets");
        ce = Calendar.getInstance();
        arrayList = new ArrayList<>();
        sessionManager = new SessionManager(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Booking...");
        pd.setCancelable(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        intiWidgets();
        Intent intent = getIntent();
        String str = intent.getStringExtra("check");
        if (str.equalsIgnoreCase("ok")) {
            int position = intent.getIntExtra("position", 0);
            ticketsModel = HomeFragment.buyTicketsModelList.get(position);
            setDataThroughIntent(ticketsModel);

        } else {
            Toast.makeText(this, "Data is Null", Toast.LENGTH_SHORT).show();
        }
        JSONArray array = ticketsModel.getJsonArray();
        for (int i = 0; i < array.length(); i++) {
            try {
                arrayList.add(array.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        miliSecsDate = milliseconds(ticketsModel.getEndDate());
        Log.e("miliSecsDate", " = " + miliSecsDate);
        ticketPrice = Integer.parseInt(ticketsModel.getPrice());


    }

    @Override
    protected void onResume() {
        super.onResume();
        getPointAndMoney();


    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }

    private void intiWidgets() {
        imageView = (ImageView) findViewById(R.id.imageView);
        tvDoller = (TextView) findViewById(R.id.tv_dollr);
        tvName = (TextView) findViewById(R.id.tv_name);
        tvDescription = (TextView) findViewById(R.id.tv_descroption);
        tvWork = (TextView) findViewById(R.id.tv_works);
        tvTypeName = (TextView) findViewById(R.id.tv_type_name);
        btnPurchase = (Button) findViewById(R.id.btn_purchase);
        btnPurchase.setOnClickListener(this);

    }

    private void setDataThroughIntent(TicketsModel ticketsModel) {
        Picasso.with(getApplicationContext())
                .load(ticketsModel.getImageUrl())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(imageView);
        tvDoller.setText("$" + ticketsModel.getPrice());
        tvName.setText(ticketsModel.getName());
        tvTypeName.setText(ticketsModel.getCategory());
        tvDescription.setText(ticketsModel.getDetails());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_purchase:
                dialog = new Dialog(this);
                // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.bookticket);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setAttributes(lp);
                dialog.setCancelable(true);
                ticketPrice = Integer.parseInt(ticketsModel.getPrice());
                tvDate = (TextView) dialog.findViewById(R.id.tv_date);
                tvTime = (TextView) dialog.findViewById(R.id.tv_time);
                final Spinner spinner = (Spinner) dialog.findViewById(R.id.spinner);
                adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, arrayList);
                spinner.setAdapter(adapter);
                etNoOfTickets = (EditText) dialog.findViewById(R.id.et_no_of_tickets);
                linearPlus = (LinearLayout) dialog.findViewById(R.id.linear_plus);
                linearMinus = (LinearLayout) dialog.findViewById(R.id.linear_minus);
                ivPlus = (ImageView) dialog.findViewById(R.id.iv_plus);
                ivMinus = (ImageView) dialog.findViewById(R.id.iv_minus);
                btnBookTicket = (Button) dialog.findViewById(R.id.btn_book_ticket);
                btnBookTicket.setEnabled(true);


                tvDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (miliSecsDate >= ce.getTimeInMillis()) {
                            showDatePicker();

                        } else {
                            Toast.makeText(BuyTicketsActivity.this, "Ticket is Expire", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        int index = parent.getSelectedItemPosition();
                        ((TextView) spinner.getSelectedView()).setTextColor(getResources().getColor(R.color.Black));
                        ((TextView) parent.getChildAt(0)).setTextSize(17);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                linearPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (noOfTicket <= 9) {
                            noOfTicket = noOfTicket + 1;
                            etNoOfTickets.setText("" + noOfTicket);
                        }


                    }
                });

                linearMinus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (noOfTicket >= 1) {
                            noOfTicket = noOfTicket - 1;
                            etNoOfTickets.setText("" + noOfTicket);
                        }


                    }
                });


                btnBookTicket.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            String time = spinner.getSelectedItem().toString();
                            String noOFtICKES = etNoOfTickets.getText().toString();
                            if (cash < ticketPrice || cash == 0) {
                                Toast.makeText(getApplicationContext(), "InSufficient Balance", Toast.LENGTH_SHORT).show();

                            } else if (tvDate.getText().toString().equalsIgnoreCase("") || tvDate.getText().toString().length() < 1) {
                                Toast.makeText(BuyTicketsActivity.this, "Please Select Show Date", Toast.LENGTH_SHORT).show();

                            } else if (time.equalsIgnoreCase("") || time.length() < 1) {
                                Toast.makeText(BuyTicketsActivity.this, "Please Select Show Time", Toast.LENGTH_SHORT).show();

                            } else if (noOFtICKES.length() < 0 || noOFtICKES.equalsIgnoreCase("") || noOFtICKES.equalsIgnoreCase("0")) {
                                Toast.makeText(BuyTicketsActivity.this, "How many Tickets You Want?", Toast.LENGTH_SHORT).show();

                            }else if(getNoOfTicket()){
                                Toast.makeText(BuyTicketsActivity.this, "You don't have Money to Purchase"+" "+ noOFtICKES+" "+"Tickets", Toast.LENGTH_LONG).show();

                            } else {
                                bookTicket(time, noOFtICKES);
                            }
                        }catch (Exception e){
                            Toast.makeText(BuyTicketsActivity.this, "Ticket is Expire", Toast.LENGTH_SHORT).show();

                        }



                    }
                });


                dialog.show();
                break;
        }


    }


    public long milliseconds(String date) {
        //String date_ = date;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            Log.e("Date in milli", " = " + timeInMilliseconds);

            return timeInMilliseconds;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    private void showDatePicker() {


        mYear = ce.get(Calendar.YEAR);
        mMonth = ce.get(Calendar.MONTH);
        mDay = ce.get(Calendar.DAY_OF_MONTH);
        datePickerDialogE = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // String year =

                        String day = dayOfMonth + "";
                        String month = (monthOfYear + 1) + "";

                        if (day.length() <= 1) {
                            day = "0" + day;
                        }

                        if (month.length() <= 1) {
                            month = "0" + month;
                        }

                        eventDate = day + "/" + month + "/" + year;
                        //Toast.makeText(SendReceiveHistoryActivity.this,""+date,Toast.LENGTH_LONG).show();
                        tvDate.setText(eventDate + "");

                    }
                }, mYear, mMonth, mDay);
        datePickerDialogE.getDatePicker().setMinDate(ce.getTimeInMillis());
        datePickerDialogE.getDatePicker().setMaxDate(miliSecsDate);// TODO: used to hide future date,month and year

        datePickerDialogE.show();

    }


    private boolean getNoOfTicket(){
         totalMoney   = ticketPrice * noOfTicket;
        if(totalMoney > cash){
            return true;

        }

        return false;

    }


    private void bookTicket(String time, String noOfTicket) {
        pd.show();
        cash = 0;

        RequestQueue requestQueue = Volley.newRequestQueue(BuyTicketsActivity.this);

        JSONObject params = new JSONObject();
        try {
            params.put("key", AppConstant.KEY);
            params.put("cardNo", sessionManager.getKeyCardNumber());
            params.put("ticketId", ticketsModel.getTicketId());
            params.put("date", tvDate.getText().toString());
            params.put("timeslot", time);
            params.put("noOfTickets", noOfTicket);

            Log.e("paramsBookTickets", params + "");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, AppConstant.BOOK_TICKET, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("responseee", response + " ");
                try {

                    JSONObject obj = new JSONObject(String.valueOf(response));
                    JSONObject jsonObjectHeader = obj.getJSONObject("header");
                    String errorCode = jsonObjectHeader.optString("ErrorCode");
                    String errorMessage = jsonObjectHeader.optString("ErrorMessage");

                    if (errorCode.equals("0")) {
                        pd.dismiss();
                        dialog.dismiss();
                        Intent intent = new Intent(BuyTicketsActivity.this, TicketBookedConfirmationActivity.class);
                        intent.putExtra("ticketAmount", totalMoney);
                        Log.e("ticketAmount",totalMoney+"");
                        startActivity(intent);
                        finish();


                    } else {
                        Toast.makeText(BuyTicketsActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                        dialog.dismiss();


                    }


                } catch (JSONException e) {
                    pd.dismiss();
                    dialog.dismiss();

                    e.printStackTrace();
                    Toast.makeText(BuyTicketsActivity.this, "JSONException" + e, Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();

                error.printStackTrace();
                Toast.makeText(BuyTicketsActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }


        };

        requestQueue.add(jsonObjectRequest);

    }

    private void getPointAndMoney() {
        pd.show();
        String url =  AppConstant.GET_POINT;
        String key = AppConstant.KEY;
        RequestQueue requestQueue = Volley.newRequestQueue(BuyTicketsActivity.this);

        JSONObject params = new JSONObject();
        try {
            params.put("key", key);
            params.put("cardno", sessionManager.getKeyCardNumber());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("pointinticket",response+"");
                    JSONObject objResponce = new JSONObject(String.valueOf(response));
                    JSONObject objHeader = objResponce.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errorMessage = objHeader.getString("ErrorMessage");
                    if(errorCode.equalsIgnoreCase("0")){
                        JSONObject jsonObjectResponse = objResponce.getJSONObject("response");

                        JSONObject objMyPoints = jsonObjectResponse.getJSONObject("myPoints");
                        String totalCash = objMyPoints.getString("totalCash");
                        String totalPoints = objMyPoints.getString("totalPoints");
                        sessionManager.savePoints(totalCash, totalPoints);
                        pd.dismiss();
                        try {
                            cash = Double.parseDouble(sessionManager.getKeyTotalCash());
                            Log.e("cash",cash+"");

                        }catch (Exception e){
                            Toast.makeText(BuyTicketsActivity.this,"Exc"+e,Toast.LENGTH_SHORT).show();

                        }
                    }else if(errorCode.equalsIgnoreCase("1005")){
                        Toast.makeText(BuyTicketsActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        pd.dismiss();
                    }

                    //sessionManager.savetransferPoint(totalCash, totalPoints);




                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(BuyTicketsActivity.this, "Exception"+ e, Toast.LENGTH_LONG).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                error.printStackTrace();
                Toast.makeText(BuyTicketsActivity.this, "VolleyError"+ error, Toast.LENGTH_LONG).show();

            }
        })

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        requestQueue.add(jsonObjectRequest);

    }

}
