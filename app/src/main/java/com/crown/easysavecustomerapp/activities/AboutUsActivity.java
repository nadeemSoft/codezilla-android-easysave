package com.crown.easysavecustomerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.utils.SessionManager;

/**
 * Created by codezilla-121 on 24/3/18.
 */

public class AboutUsActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout linearAddress, linearEmail, linearPhone;
    TextView tvAddress, tvEmail, tvPhone;
    SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        sessionManager=new SessionManager(this);
        this.setTitle("About Us");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initWidgets();
        String email = sessionManager.getKeyEmail();
        //tvEmail.setText(email + "");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.address_linear:
                break;
            case R.id.email_linear:
                break;
            //case R.id.phone_linear:
             //   break;
        }
    }

    private void initWidgets() {
        linearAddress = (LinearLayout) findViewById(R.id.address_linear);
        linearAddress.setOnClickListener(this);
        linearEmail = (LinearLayout) findViewById(R.id.email_linear);
        linearEmail.setOnClickListener(this);
        //linearPhone = (LinearLayout) findViewById(R.id.phone_linear);
        //linearPhone.setOnClickListener(this);

        //tvAddress = (TextView) findViewById(R.id.tv_address);

        //tvEmail = (TextView) findViewById(R.id.tv_email);
        //tvPhone = (TextView) findViewById(R.id.tv_phone);
    }
    @Override
    public boolean onSupportNavigateUp() {

       // onBackPressed();
        Intent intent = new Intent(AboutUsActivity.this,NavigationDrawerActivity.class);
        startActivity(intent);
        return true;
    }

}
