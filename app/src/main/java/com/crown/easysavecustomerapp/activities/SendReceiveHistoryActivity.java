package com.crown.easysavecustomerapp.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.fragments.ReceiveHistoryFragment;
import com.crown.easysavecustomerapp.fragments.SendHistoryFragment;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by codezilla-10 on 23/2/18.
 */

public class SendReceiveHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    TabLayout tabLayout;
    ViewPager viewPager;
    ProgressDialog pd;
    TextView tvStartDate, tvEndDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String startDate = "01/02/2018", endDate = "10/02/2018";
    ViewPagerAdapter adapter;
    public static JSONObject jsonObjDateSend;
    public static JSONObject jsonObjDatReceive;

    SessionManager sessionManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_receive_history_layout);
        sessionManager = new SessionManager(this);
        this.setTitle("Transfer History");
        pd = new ProgressDialog(SendReceiveHistoryActivity.this);
        pd.setMessage("Loading..");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tvStartDate = (TextView) findViewById(R.id.tv_start_date);
        tvStartDate.setOnClickListener(this);
        tvEndDate = (TextView) findViewById(R.id.tv_end_date);
        tvEndDate.setOnClickListener(this);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        tvStartDate.setText(sdf.format(cal.getTime()));
        startDate = sdf.format(cal.getTime());
        tvEndDate.setText(sdf.format(c.getTime()));
        endDate = sdf.format(c.getTime());
        getHistory();


        // getHistory();
//        tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).setCustomView(R.layout.send_receive_history_layout);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_start_date:
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // String year =

                                String day = dayOfMonth + "";
                                String month = (monthOfYear + 1) + "";

                                if (day.length() <= 1) {
                                    day = "0" + day;
                                }

                                if (month.length() <= 1) {
                                    month = "0" + month;
                                }

                                startDate = day + "/" + month + "/" + year;
                                //Toast.makeText(SendReceiveHistoryActivity.this,""+date,Toast.LENGTH_LONG).show();
                                tvStartDate.setText(startDate);

                                if (!endDate.isEmpty()) {
                                    getHistory();

                                } else {
                                    Toast.makeText(SendReceiveHistoryActivity.this, "Please Select End Date", Toast.LENGTH_LONG).show();
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;

            case R.id.tv_end_date:
                final Calendar ce = Calendar.getInstance();
                mYear = ce.get(Calendar.YEAR);
                mMonth = ce.get(Calendar.MONTH);
                mDay = ce.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialoge = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String day = dayOfMonth + "";
                                String month = (monthOfYear + 1) + "";

                                if (day.length() <= 1) {
                                    day = "0" + day;
                                }

                                if (month.length() <= 1) {
                                    month = "0" + month;
                                }

                                endDate = day + "/" + month + "/" + year;
                                tvEndDate.setText(endDate + "");

                                if (!startDate.isEmpty()) {
                                    getHistory();

                                } else {
                                    Toast.makeText(SendReceiveHistoryActivity.this, "Please Select Start Date", Toast.LENGTH_LONG).show();
                                }


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialoge.show();
                break;

        }
    }

    private void setupViewPager(ViewPager viewPager) {

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new SendHistoryFragment(), "Sent History");
        adapter.addFragment(new ReceiveHistoryFragment(), "Receive History");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void getHistory() {
        pd.show();
        Log.e("startingdate", startDate + "");
        Log.e("enddate", endDate + "");

        // String Url = "http://dev.easysavecustomer.com/EasySaveCustomerAPI.svc/transferhistory?key=" + AppConstant.KEY + "&cardNo=" + sessionManager.getKeyCardNumber() + "&start=" + startDate + "&end=" + endDate;
        String Url = AppConstant.BASE_URL_GET + "transferhistory?key=" + AppConstant.KEY + "&cardNo=" + sessionManager.getKeyCardNumber() + "&start=" + startDate + "&end=" + endDate;

        Log.e("chekurl", Url);

        StringRequest request = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response + "");
                 jsonObjDateSend = new JSONObject();
                jsonObjDatReceive = new JSONObject();

                try {
                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");

                    if (!errorCode.equals("1005")) {
                        JSONObject objResponseD = objResponse.getJSONObject("response");
                        JSONArray objReciev = objResponseD.getJSONArray("recievedHistory");
                        Log.e("recievedHistory", objReciev + "");


                        for (int i = 0; i < objReciev.length(); i++) {
                            JSONObject objReceivedata = objReciev.getJSONObject(i);
                            String amount = objReceivedata.getString("amount");
                            String recieverName = objReceivedata.getString("recieverName");
                            String senderName = objReceivedata.getString("senderName");
                            String transDate = objReceivedata.getString("transDate");
                            String transTime = objReceivedata.getString("transTime");

                            if (!jsonObjDatReceive.has(objReceivedata.getString("transDate"))) {
                                JSONArray jsonArray = new JSONArray();
                                jsonArray.put(objReceivedata);
                                jsonObjDatReceive.put(objReceivedata.getString("transDate"), jsonArray);


                            } else {
                                jsonObjDatReceive.getJSONArray(objReceivedata.getString("transDate")).put(objReceivedata);

                            }



                        }

                        JSONArray arrSend = objResponseD.getJSONArray("sentHistory");
                        Log.e("sentHistory", arrSend + "");

                        for (int i = 0; i < arrSend.length(); i++) {
                            JSONObject objSend = arrSend.getJSONObject(i);
                            String amount = objSend.getString("amount");
                            String recieverName = objSend.getString("recieverName");
                            String senderName = objSend.getString("senderName");
                            String transDate = objSend.getString("transDate");
                            String transTime = objSend.getString("transTime");

                            if (!jsonObjDateSend.has(objSend.getString("transDate"))) {
                                JSONArray jsonArray = new JSONArray();
                                jsonArray.put(objSend);
                                jsonObjDateSend.put(objSend.getString("transDate"), jsonArray);


                            } else {
                                jsonObjDateSend.getJSONArray(objSend.getString("transDate")).put(objSend);

                            }


                        }
                        Log.e("createJsonReceive",jsonObjDatReceive+"");
                        Log.e("createJsonSend",jsonObjDateSend+"");


                        setupViewPager(viewPager);
                        adapter.notifyDataSetChanged();
                        pd.dismiss();

                    } else {
                        setupViewPager(viewPager);
                        adapter.notifyDataSetChanged();
                        pd.dismiss();

                    }


                } catch (Exception e) {
                    pd.dismiss();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();


            }
        }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(SendReceiveHistoryActivity.this);
        requestQueue.add(request);

    }
}
