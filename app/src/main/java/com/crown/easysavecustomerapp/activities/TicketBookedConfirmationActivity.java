package com.crown.easysavecustomerapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;

public class TicketBookedConfirmationActivity extends AppCompatActivity implements View.OnClickListener {
    int priceTicket;
    TextView tvPrice;
    Button btnSendMoney,btnPayBill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_booked_confirmation);
        this.setTitle("Payment Successful");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        priceTicket = intent.getIntExtra("ticketAmount", 0);
        tvPrice = (TextView) findViewById(R.id.paymentAmount);
        tvPrice.setText("$" + priceTicket);
        btnSendMoney = (Button) findViewById(R.id.btn_send_money_confirmation);
        btnSendMoney.setOnClickListener(this);
        btnPayBill = (Button) findViewById(R.id.btn_pay_bill);
        btnPayBill.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {

        //onBackPressed();
        finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send_money_confirmation:
                Intent intentSend = new Intent(TicketBookedConfirmationActivity.this, SendMoneyActivity.class);
                startActivity(intentSend);
                finish();
                break;
            case R.id.btn_pay_bill:
                Intent intentPayBill = new Intent(TicketBookedConfirmationActivity.this, PaySeeAllActivity.class);
                startActivity(intentPayBill);
                finish();
                break;
        }

    }
}
