package com.crown.easysavecustomerapp.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.PayCompanyAdapter;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.models.BillerCompany;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class PayCompanyActivity extends AppCompatActivity {
    String value = null;
    RecyclerView rvPayCompany;
    PayCompanyAdapter payCompanyAdapter;
    LinearLayoutManager linearLayoutManager;
    public static PayCompanyActivity payCompanyActivity;
    ArrayList<BillerCompany> billerCompanyList = null;

    ProgressDialog pd = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_company);
        this.setTitle("Pay a Company");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        value = getIntent().getExtras().getString("value");


        Log.e("clickValue in Pay", value + "");

        pd = new ProgressDialog(PayCompanyActivity.this);
        pd.setMessage("Loading");
        pd.setCancelable(false);

        billerCompanyList = new ArrayList<>();
        payCompanyActivity = this;
        rvPayCompany = (RecyclerView) findViewById(R.id.rv_pay_company);

        getBiller();


    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }

    private void getBiller() {
        pd.show();
        // http://api.easysavemerchants.com/EasySaveAPI.svc/biller?key=EASYSAVEAPIV1-10001&serviceId=3

        String url = AppConstant.BASE_URL_BILL + "biller?key=EASYSAVEAPIV1-10001" + "&serviceId=" + value;
        Log.e("url ", url + "");

        // String url = AppConstant.GET_BILLER +AppConstant.KEY + "&serviceId=" +value;

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("biller", response + "");
                billerCompanyList.clear();
                try {
                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errorMessage = objHeader.getString("ErrorMessage");
                    if (errorCode.equalsIgnoreCase("0")) {
                        JSONArray jsonArraybiller = objResponse.getJSONArray("biller");
                        for (int i = 0; i < jsonArraybiller.length(); i++) {
                            JSONObject obj = jsonArraybiller.getJSONObject(i);
                            String billerImage = obj.getString("BillerImage");
                            String billerId = obj.getString("Biller_ID");
                            String billerCategory = obj.getString("Category");
                            String billerCompanyName = obj.getString("CompanyName");

                            BillerCompany billerCompany = new BillerCompany(billerImage, billerId, billerCategory, billerCompanyName);
                            billerCompanyList.add(billerCompany);
                        }
                        payCompanyAdapter = new PayCompanyAdapter(getApplicationContext(), billerCompanyList);
                        linearLayoutManager = new LinearLayoutManager(PayCompanyActivity.this, LinearLayoutManager.VERTICAL, false);
                        rvPayCompany.setLayoutManager(linearLayoutManager);
                        rvPayCompany.setAdapter(payCompanyAdapter);

                        pd.dismiss();

                    } else if (!errorCode.equalsIgnoreCase("0")) {
                        pd.dismiss();
                        Toast.makeText(PayCompanyActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();

                    }


                } catch (Exception e) {

                    pd.dismiss();
                    Toast.makeText(PayCompanyActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }


        }   , new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(PayCompanyActivity.this, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(PayCompanyActivity.this);
        requestQueue.add(request);

    }
}
