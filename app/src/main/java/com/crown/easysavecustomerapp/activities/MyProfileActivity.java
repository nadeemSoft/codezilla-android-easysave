package com.crown.easysavecustomerapp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.fragments.HomeFragment;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-10 on 27/2/18.
 */

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etName, etMobileNo, etEmailId;
    TextView tvBalance, tvChangePwd;
    Button btnSave;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    SessionManager sessionManager;
    ProgressDialog pd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_myprofile_layout);
        sessionManager = new SessionManager(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findWidgets();

        setTitle("My Profile");
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        getProfile();

    }

    private void findWidgets() {

        etName = (EditText)findViewById(R.id.et_name);
        etMobileNo = (EditText)findViewById(R.id.et_mobileNo);
        etEmailId = (EditText)findViewById(R.id.et_emailId);
        tvBalance = (TextView) findViewById(R.id.tv_balance);
        tvChangePwd = (TextView)findViewById(R.id.tv_changePwd);
        tvChangePwd.setOnClickListener(this);
        btnSave = (Button)findViewById(R.id.btn_save_changes);
        btnSave.setOnClickListener(this);

    }

    private void getFirstLastName(String name) {
        pd.show();
        String lastnam = "";
        String firstnam = "";
        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) == ' ') {
                lastnam = name.substring(i + 1);
                firstnam = name.substring(0, i);
                Log.e("FirstName", firstnam);
                Log.e("lastnam", lastnam);


            }

        }

        String mailId = etEmailId.getText().toString().trim();

        if(etName.getText().toString().length() > 0 && etMobileNo.getText().toString().length() > 0 && etMobileNo.getText().toString().length() == 10  && !etMobileNo.getText().toString().equalsIgnoreCase("null") && mailId.matches(emailPattern)  && !etEmailId.getText().toString().equalsIgnoreCase("null")  && !etName.getText().toString().equalsIgnoreCase("null")){
            UpdateProfile(firstnam, lastnam);


        }else {
            pd.dismiss();
            Toast.makeText(MyProfileActivity.this,"Something is Wrong",Toast.LENGTH_SHORT).show();

        }


    }
    private void getProfile() {
        pd.show();

        // String url ="http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/MyProfile?key=EASYSAVECUSTOMERAPIV1-10001&cardNo=728302770208";
        String url = AppConstant.BASE_URL_GET+"MyProfile?key=" + AppConstant.KEY + "&cardNo=" + sessionManager.getKeyCardNumber();

        RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("getProfileResponse",response);
                if (response != null) {

                    try {
                        JSONObject json = new JSONObject(response);

                        JSONObject jsonObjectHeader = json.getJSONObject("header");
                        String errorCode = jsonObjectHeader.optString("ErrorCode");
                        String errorMessage = jsonObjectHeader.optString("ErrorMessage");

                        if (errorCode.equals("0")) {
                            JSONObject jsonObject = json.getJSONObject("response");

                            Log.e("responseGetProfile", response + "");

                            String CustomerID = jsonObject.optString("Customer_ID");
                            String cardNo = jsonObject.optString("cardNo");
                            String country = jsonObject.optString("country");
                            String email = jsonObject.optString("email");
                            String firstName = jsonObject.optString("firstName");
                            String lastName = jsonObject.optString("lastName");
                            String mobile = jsonObject.optString("mobile");

                            etName.setText("" + firstName + " " + lastName);
                            etMobileNo.setText("" + mobile);
                            etEmailId.setText("" + email);
                            tvBalance.setText("$" + sessionManager.getKeyTotalCash());

                            pd.dismiss();


                        } else {
                            Toast.makeText(MyProfileActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                            pd.dismiss();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        pd.dismiss();
                        Toast.makeText(MyProfileActivity.this, "JSONException" + e, Toast.LENGTH_LONG).show();

                    }

                }else {
                    pd.dismiss();
                    Toast.makeText(MyProfileActivity.this, "Response" + response, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MyProfileActivity.this, "VolleyError" +  error, Toast.LENGTH_LONG).show();



            }
        });


        requestQueue.add(request);

    }


    private void UpdateProfile(String firstName, String lastName) {

        JSONObject params = new JSONObject();
        try {
            params.put("key", AppConstant.KEY);
            params.put("cardNo", sessionManager.getKeyCardNumber());
            params.put("country", sessionManager.getKeyCountry());
            params.put("email", etEmailId.getText().toString());
            params.put("firstName", firstName);
            params.put("lastName", lastName);
            params.put("mobile", etMobileNo.getText().toString());

            Log.e("UpadeProfileParams",params +"");


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(MyProfileActivity.this, "JSONException " + e, Toast.LENGTH_SHORT).show();

        }

        RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, AppConstant.UPDATE_PROFILE, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {

                    try {
                        JSONObject json = new JSONObject(String.valueOf(response));

                        JSONObject jsonObjectHeader = json.getJSONObject("header");
                        String errorCode = jsonObjectHeader.optString("ErrorCode");
                        String errorMessage = jsonObjectHeader.optString("ErrorMessage");

                        if (errorCode.equals("0")) {
                            JSONObject jsonObject = json.getJSONObject("response");

                            Log.e("responseUpdateProfile", response + "");

                            String CustomerID = jsonObject.optString("Customer_ID");
                            String cardNo = jsonObject.optString("cardNo");
                            String country = jsonObject.optString("country");
                            String email = jsonObject.optString("email");
                            String firstName = jsonObject.optString("firstName");
                            String lastName = jsonObject.optString("lastName");
                            String mobile = jsonObject.optString("mobile");

                            if(cardNo.equalsIgnoreCase("null")){
                                sessionManager.saveCountry(country);
                                sessionManager.saveUpdatePrifile(CustomerID,firstName,lastName,email,mobile);
                            }else {
                                sessionManager.saveCountry(country);
                                sessionManager.createLoginSession(CustomerID,firstName,lastName,"dob",cardNo,email,mobile);


                            }

                            Toast.makeText(MyProfileActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                            Fragment homeFragment = new HomeFragment();
                            FragmentTransaction fragmentTransactionHome = MyProfileActivity.this.getSupportFragmentManager().beginTransaction();
                            fragmentTransactionHome.replace(R.id.content_frame, homeFragment);
                            fragmentTransactionHome.commit();
                            pd.dismiss();


                        } else {
                            Toast.makeText(MyProfileActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                            pd.dismiss();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        pd.dismiss();
                        Toast.makeText(MyProfileActivity.this, "JSONException" + e, Toast.LENGTH_LONG).show();

                    }

                }else {
                    pd.dismiss();
                    Toast.makeText(MyProfileActivity.this, "Response" + response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pd.dismiss();
                Toast.makeText(MyProfileActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        })


        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");

                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);

    }


    private void showDialog() {
        final Dialog dialog1 = new Dialog(MyProfileActivity.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.change_password);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog1.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog1.getWindow().setAttributes(lp);

        EditText etCurrentPassword = (EditText) dialog1.findViewById(R.id.et_currentPW);
        etCurrentPassword.setText(sessionManager.getKeyPassword());
        final EditText etNewPassword = (EditText) dialog1.findViewById(R.id.et_newPW);
        final EditText etConfirmPassword = (EditText) dialog1.findViewById(R.id.et_confirmPW);
        Button btnSave = (Button) dialog1.findViewById(R.id.btn_change_passwprd);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNewPassword.getText().toString().length() > 0 && etConfirmPassword.getText().toString().length() > 0 && etNewPassword.getText().toString().equalsIgnoreCase(etConfirmPassword.getText().toString())) {
                    pd.show();
                    JSONObject params = new JSONObject();
                    try {
                        params.put("key", AppConstant.KEY);
                        params.put("cardNo", sessionManager.getKeyCardNumber());
                        params.put("password", etConfirmPassword.getText().toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(MyProfileActivity.this, "JSONException " + e, Toast.LENGTH_SHORT).show();

                    }

                    RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);


                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, AppConstant.CHANGE_PASSWORD, params, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (response != null) {
                                try {
                                    Log.e("response", response + " ");

                                    JSONObject objResponse = new JSONObject(response.toString());
                                    Log.e("objResponse", objResponse + " ");

                                    JSONObject obj = objResponse.getJSONObject("header");
                                    Log.e("obj", obj + " ");

                                    String ErrorCode = obj.getString("ErrorCode");
                                    String ErrorMessage = obj.getString("ErrorMessage");
                                    if (ErrorCode.equalsIgnoreCase("0")) {
                                        sessionManager.savePassword(etConfirmPassword.getText().toString());

                                        Toast.makeText(MyProfileActivity.this, "" + ErrorMessage, Toast.LENGTH_SHORT).show();
                                        Fragment homeFragment = new HomeFragment();
                                        FragmentTransaction fragmentTransactionHome = MyProfileActivity.this.getSupportFragmentManager().beginTransaction();
                                        fragmentTransactionHome.replace(R.id.content_frame, homeFragment);
                                        fragmentTransactionHome.commit();
                                        pd.dismiss();
                                        dialog1.dismiss();


                                    } else {
                                        pd.dismiss();
                                        Toast.makeText(MyProfileActivity.this, "" + ErrorMessage, Toast.LENGTH_SHORT).show();

                                    }


                                } catch (Exception e) {
                                    pd.dismiss();
                                    Toast.makeText(MyProfileActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();


                                }

                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            pd.dismiss();
                            Toast.makeText(MyProfileActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();


                        }
                    })


                    {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<String, String>();
                            headers.put("Content-Type", "application/json; charset=utf-8");return headers;
                        }

                    };


                    requestQueue.add(jsonObjectRequest);

                }
            }
        });


        dialog1.setCancelable(true);
        dialog1.show();


    }


    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_changePwd:
                showDialog();
                break;
            case R.id.btn_save_changes:
                String name = etName.getText().toString();
                getFirstLastName(name);
                break;
        }

    }
}
