package com.crown.easysavecustomerapp.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.HistoryLoadingAdapter;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.models.BillHistoryModel;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by codezilla-10 on 27/2/18.
 */

public class LoadingHistoryActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvStartDate, tvEndDate;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String startDate = "29/04/2018", endDate = "05/05/2018";
    ProgressDialog pd;
    ArrayList<BillHistoryModel> billHistoryList = null;
    RecyclerView rvBillHistory;
    LinearLayout linearLayoutNoHistory;
    HistoryLoadingAdapter historyLoadingAdapter;
    SessionManager sessionManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_history);
        sessionManager = new SessionManager(this);
        this.setTitle("Loading History");
        findWidgets();
        billHistoryList = new ArrayList<>();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        pd = new ProgressDialog(LoadingHistoryActivity.this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        tvStartDate.setText(sdf.format(cal.getTime()));
        startDate = sdf.format(cal.getTime());
        tvEndDate.setText(sdf.format(c.getTime()));
        endDate = sdf.format(c.getTime());
        getLoadHistory();


    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }


    private void findWidgets() {
        rvBillHistory = (RecyclerView) findViewById(R.id.rv_bill_history);
        tvStartDate = (TextView) findViewById(R.id.tv_start_date);
        tvStartDate.setOnClickListener(this);
        linearLayoutNoHistory = (LinearLayout) findViewById(R.id.linear_no_history);
        tvEndDate = (TextView) findViewById(R.id.tv_end_date);
        tvEndDate.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_start_date:
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // String year =

                                String day = dayOfMonth + "";
                                String month = (monthOfYear + 1) + "";

                                if (day.length() <= 1) {
                                    day = "0" + day;
                                }

                                if (month.length() <= 1) {
                                    month = "0" + month;
                                }

                                startDate = day + "/" + month + "/" + year;
                                //Toast.makeText(SendReceiveHistoryActivity.this,""+date,Toast.LENGTH_LONG).show();
                                tvStartDate.setText(startDate + "");

                                if (!endDate.isEmpty()) {
                                    getLoadHistory();
                                } else {
                                    Toast.makeText(LoadingHistoryActivity.this, "Please Select End Date", Toast.LENGTH_LONG).show();
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;

            case R.id.tv_end_date:
                final Calendar ce = Calendar.getInstance();
                mYear = ce.get(Calendar.YEAR);
                mMonth = ce.get(Calendar.MONTH);
                mDay = ce.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialogE = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // String year =

                                String day = dayOfMonth + "";
                                String month = (monthOfYear + 1) + "";

                                if (day.length() <= 1) {
                                    day = "0" + day;
                                }

                                if (month.length() <= 1) {
                                    month = "0" + month;
                                }

                                endDate = day + "/" + month + "/" + year;
                                //Toast.makeText(SendReceiveHistoryActivity.this,""+date,Toast.LENGTH_LONG).show();
                                tvEndDate.setText(endDate + "");
                                if (!startDate.isEmpty()) {
                                    getLoadHistory();
                                } else {
                                    Toast.makeText(LoadingHistoryActivity.this, "Please Select Start Date", Toast.LENGTH_LONG).show();
                                }

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialogE.show();
                break;

        }
    }


    public void getLoadHistory() {
        pd.show();
        Log.e("startDate", startDate + "");
        Log.e("endDate", endDate + "");

        //String url = "http://api.easysavecustomer.com/EasySaveCustomerAPI.svc/loadhistory?key=EASYSAVECUSTOMERAPIV1-10001&cardNo=728302770208&start=01/02/2018&end=10/02/2018"
        String Url = AppConstant.BASE_URL_GET + "loadhistory?key=" + AppConstant.KEY + "&cardNo=" + sessionManager.getKeyCardNumber() + "&start=" + startDate + "&end=" + endDate;
        Log.e("chekurl", Url);

        StringRequest request = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response", response + "");
                billHistoryList.clear();
                JSONObject jsonObjDate = new JSONObject();

                try {

                    JSONObject objResponse = new JSONObject(response);
                    JSONObject objHeader = objResponse.getJSONObject("header");
                    String errorCode = objHeader.getString("ErrorCode");
                    String errormessage = objHeader.getString("ErrorMessage");

                    if (!errorCode.equals("1005")) {
                        linearLayoutNoHistory.setVisibility(View.GONE);
                        JSONArray arrResponse = objResponse.getJSONArray("response");
                        Log.e("LoadingHistoryResponse", arrResponse + "");

                        for (int i = 0; i < arrResponse.length(); i++) {
                            JSONObject objReceivedata = arrResponse.getJSONObject(i);
                            String amount = objReceivedata.getString("amount");
                            //String companyName = objReceivedata.getString("companyName");
                            String transDate = objReceivedata.getString("transDate");
                            String transTime = objReceivedata.getString("transTime");

                            if(!jsonObjDate.has(objReceivedata.getString("transDate"))){
                                JSONArray jsonArray = new JSONArray();
                                jsonArray.put(objReceivedata);
                                jsonObjDate.put(objReceivedata.getString("transDate"),jsonArray);


                            }else {
                                jsonObjDate.getJSONArray(objReceivedata.getString("transDate")).put(objReceivedata);

                            }
                          /*  BillHistoryModel billHistoryModel = new BillHistoryModel(amount, companyName, transDate, transTime);
                            billHistoryList.add(billHistoryModel);*/


                        }
                        Log.e("createJsonDataDate",jsonObjDate+"");
                        historyLoadingAdapter = new HistoryLoadingAdapter(getApplicationContext(), jsonObjDate);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LoadingHistoryActivity.this, LinearLayoutManager.VERTICAL, false);
                        rvBillHistory.setLayoutManager(linearLayoutManager);
                        rvBillHistory.setAdapter(historyLoadingAdapter);
                        historyLoadingAdapter.notifyDataSetChanged();
                        pd.dismiss();


                       /* historyLoadingAdapter = new HistoryLoadingAdapter(getApplicationContext(), billHistoryList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LoadingHistoryActivity.this, LinearLayoutManager.VERTICAL, false);
                        rvBillHistory.setLayoutManager(linearLayoutManager);
                        rvBillHistory.setAdapter(historyLoadingAdapter);
                        historyLoadingAdapter.notifyDataSetChanged();
                        pd.dismiss();*/


                    } else {
                        pd.dismiss();
                        linearLayoutNoHistory.setVisibility(View.VISIBLE);

                    }


                } catch (Exception e) {
                    pd.dismiss();


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();


            }
        }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(LoadingHistoryActivity.this);
        requestQueue.add(request);

    }
}
