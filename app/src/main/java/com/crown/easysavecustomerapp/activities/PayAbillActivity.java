package com.crown.easysavecustomerapp.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * Created by codezilla-10 on 10/3/18.
 */

public class PayAbillActivity extends AppCompatActivity implements View.OnClickListener {
    String mobileNo, amount, person, number, billerId, billerImage;
    Bitmap bitmap = null;
    ImageView ivCheck, ivBIller;

    EditText etMobileNumber, etAmount, etNamePerson, etInvoiceNo;
    LinearLayout linearImage;
    Button btnPay;
    ProgressDialog pd;
    SessionManager sessionManager;
    private Uri fileUri;
    double userHavecash;
    int amountEnter;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    public static final int MEDIA_TYPE_IMAGE = 1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_pay_a_bill_layout);
        this.setTitle("Pay a Bill");
        sessionManager = new SessionManager(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PermissionChecker.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 123);
        }
        initWidgets();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pd = new ProgressDialog(PayAbillActivity.this);
        pd.setMessage("Please Wait..");
        pd.setCancelable(false);

        billerId = getIntent().getExtras().getString("billerId");
        billerImage = getIntent().getExtras().getString("billerImage");
        Picasso.with(PayAbillActivity.this)
                .load(billerImage)
                .placeholder(R.mipmap.ic_launcher)
                .into(ivBIller);

        Log.e("paybillerId", billerId + "");


    }

    private void initWidgets() {
        ivBIller = (ImageView) findViewById(R.id.iv_biller);
        etMobileNumber = (EditText) findViewById(R.id.et_mobile_number);
        etAmount = (EditText) findViewById(R.id.et_amount);
        etNamePerson = (EditText) findViewById(R.id.et_name_of_person);
        etInvoiceNo = (EditText) findViewById(R.id.et_invoice_number);
        linearImage = (LinearLayout) findViewById(R.id.ll_photo);
        btnPay = (Button) findViewById(R.id.btn_pay);
        ivCheck = findViewById(R.id.iv_image_check);
        ivCheck.setVisibility(View.GONE);


        linearImage.setOnClickListener(this);
        btnPay.setOnClickListener(this);


    }

    @Override
    public boolean onSupportNavigateUp() {

        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_photo:
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, 111);
                break;
            case R.id.btn_pay:
                userHavecash = Double.parseDouble(sessionManager.getKeyTotalCash());
                getDataFromUser();
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 111) {
            try {
                Bundle bn = data.getExtras();
                bitmap = (Bitmap) bn.get("data");

                Log.e("image", bitmap + "");
                if (bitmap != null) {
                    ivCheck.setVisibility(View.VISIBLE);
                    // linearImage.getBackground().setColorFilter(Color.parseColor("#57a13b"), PorterDuff.Mode.SRC_ATOP);

                }

            } catch (Exception e) {
                Toast.makeText(PayAbillActivity.this, "please capture Image", Toast.LENGTH_SHORT).show();

            }


        }else {
            Toast.makeText(PayAbillActivity.this, "please give permission", Toast.LENGTH_SHORT).show();

        }

    }


    private void getDataFromUser() {
        try {
            mobileNo = etMobileNumber.getText().toString();
            amount = etAmount.getText().toString();
            person = etNamePerson.getText().toString();
            number = etInvoiceNo.getText().toString();
            amountEnter = Integer.parseInt((etAmount.getText().toString()));

            if (checkValidation()) {
                NetworkTask networkTask = new NetworkTask();
                networkTask.execute();
            }
        }catch (Exception e){
            Toast.makeText(PayAbillActivity.this, "Enter Amount", Toast.LENGTH_SHORT).show();

        }



    }

    private boolean checkValidation() {

        if (etMobileNumber.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(PayAbillActivity.this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etAmount.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(PayAbillActivity.this, "Enter Amount", Toast.LENGTH_SHORT).show();
            return false;
        }  else if (etNamePerson.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(PayAbillActivity.this, "Enter Person Name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etInvoiceNo.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(PayAbillActivity.this, "Enter Invoice Number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (userHavecash < amountEnter || userHavecash == 0) {
            Toast.makeText(PayAbillActivity.this, "InSufficient Balance", Toast.LENGTH_SHORT).show();
            return false;
        }/*else if (bitmap == null) {
            Toast.makeText(PayAbillActivity.this, "Select Invoice Image", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        return true;
    }

    class NetworkTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            pd.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            //getPayment();
            String responseStr = null;
            byte[] data = null;
            String url = "http://api.easysavecustomer.com/EasySaveCustomerBill.svc/pay";
            MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");
            Log.e("bitmap ", bitmap + "");
            // Bitmap bitmap = DateUtils.decodeBase64(encodedBitmap);
            if (bitmap != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                data = stream.toByteArray();
            }

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(20, TimeUnit.SECONDS);
            client.setReadTimeout(25, TimeUnit.SECONDS);
            RequestBody requestBody;
            if (data == null) {
                requestBody = new MultipartBuilder()
                        .type(MultipartBuilder.FORM)
                        .addFormDataPart("key", AppConstant.KEY)
                        .addFormDataPart("BillerId", billerId)
                        .addFormDataPart("cardNo", sessionManager.getKeyCardNumber())
                        .addFormDataPart("invoiceNo", number)
                        .addFormDataPart("billAmount", amount)
                        .addFormDataPart("mobileNo", mobileNo)
                        .addFormDataPart("nameOfPerson", person)
                        .addFormDataPart("invoice", "")
                        .build();

            } else {

                requestBody = new MultipartBuilder()
                        .type(MultipartBuilder.FORM)
                        .addFormDataPart("key", AppConstant.KEY)
                        .addFormDataPart("BillerId", billerId)
                        .addFormDataPart("cardNo", sessionManager.getKeyCardNumber())
                        .addFormDataPart("invoiceNo", number)
                        .addFormDataPart("billAmount", amount)
                        .addFormDataPart("mobileNo", mobileNo)
                        .addFormDataPart("nameOfPerson", person)
                        .addFormDataPart("invoice", "image.jpg", RequestBody.create(MEDIA_TYPE_JPG, data))
                        .build();

            }

            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .build();
            Response response = null;

            try {
                response = client.newCall(request).execute();
                responseStr = response.body().string().toString();
                Log.d("response.code() :" + response.code(), "");

            } catch (IOException e) {
                pd.dismiss();
                e.printStackTrace();
                Toast.makeText(PayAbillActivity.this, "IOException" + e, Toast.LENGTH_LONG).show();
            }

            return responseStr;
        }

        @Override
        protected void onPostExecute(String responseStr) {
            super.onPostExecute(responseStr);
            pd.dismiss();
            String resultResponse = new String(responseStr);
            Log.e("responseMulti", resultResponse + "");
            try {
                JSONObject objResponse = new JSONObject(resultResponse);
                JSONObject jsonObject = objResponse.getJSONObject("header");
                String errorCode = jsonObject.getString("ErrorCode");
                String errorMessage = jsonObject.getString("ErrorMessage");
                //Toast.makeText(PayAbillActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                if (errorCode.equalsIgnoreCase("0")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(PayAbillActivity.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("Pay a Bill");

                    // Setting Dialog Message
                    alertDialog.setMessage("You have successfully paid a bill!");
                    alertDialog.setCancelable(false);

                    // Setting Icon to Dialog
                    //alertDialog.setIcon(R.drawable.tick);

                    // Setting OK Button
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog closed
                            finish();

                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                    TextView messageText = alertDialog.findViewById(android.R.id.message);
                    messageText.setGravity(Gravity.CENTER);

                } else if (!errorCode.equalsIgnoreCase("0")) {
                    Toast.makeText(PayAbillActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();

                }

            } catch (JSONException e) {

            }

        }
    }
}



