package com.crown.easysavecustomerapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.CustomPagerAdapter;

/**
 * Created by codezilla-10 on 21/2/18.
 */

public class ViewPagerActivity extends AppCompatActivity implements View.OnClickListener {
    int dotscount;
    private ImageView[] dots;
    TextView tv_started;
    int images[] = {R.drawable.intro1, R.drawable.intro2,R.drawable.intro3};
    int[] text1 = {R.string.viewPager1_heading, R.string.viewPager2_heading,R.string.viewPager3_heading};
    int[] text2 = {R.string.viewPager1_subText, R.string.viewPager2_subText,R.string.viewPager3_subText};

    ViewPager viewPager;
    LinearLayout linearLayout;
    ImageView iv_next;
    CustomPagerAdapter customPagerAdapter;
    // TextView tvSkip;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_screen_layout);
        getSupportActionBar().hide();

        initWidget();
        // getSharedPref();
        //tvSkip = (TextView) findViewById(R.id.tv_skip);
        setViewPager();


    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.tv_started:

                Intent i = new Intent(this,LoginSignUpActivity.class);
                startActivity(i);
                finish();
                break;

            case R.id.iv_next:

                Intent intent = new Intent(this,LoginSignUpActivity.class);
                startActivity(intent);
                finish();

                break;
        }

    }

    private void initWidget(){

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        linearLayout = (LinearLayout) findViewById(R.id.SliderDots);
        tv_started = (TextView) findViewById(R.id.tv_started);
         iv_next = (ImageView)findViewById(R.id.iv_next);
        tv_started.setOnClickListener(this);
        iv_next.setOnClickListener(this);

    }

    private void setViewPager(){

        customPagerAdapter = new CustomPagerAdapter(ViewPagerActivity.this, images, text1, text2);
        viewPager.setAdapter(customPagerAdapter);

        dotscount = customPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            linearLayout.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.non_active_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));
                if (position == 0) {
                    linearLayout.setVisibility(View.VISIBLE);
                    tv_started.setVisibility(View.GONE);
                    iv_next.setVisibility(View.VISIBLE);

                    //Toast.makeText(getApplicationContext(), "Position" + position, Toast.LENGTH_LONG).show();

                }else if(position == 1){
                    linearLayout.setVisibility(View.VISIBLE);
                    tv_started.setVisibility(View.GONE);
                    iv_next.setVisibility(View.VISIBLE);

                    //Toast.makeText(getApplicationContext(), "Position" + position, Toast.LENGTH_LONG).show();

                }else if(position == 2){
                    linearLayout.setVisibility(View.GONE);
                    tv_started.setVisibility(View.VISIBLE);
                    iv_next.setVisibility(View.GONE);
                    //Toast.makeText(getApplicationContext(), "Position" + position, Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // Toast.makeText(getApplicationContext(),"sTATE"+state,Toast.LENGTH_LONG).show();
            }
        });

    }



    private void getSharedPref() {

/*
============================================ Get sharedpreference Data ====================================================================
*/
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String CustomerID = sharedPreferences.getString("Customer_ID", "");
        String cardNo = sharedPreferences.getString("cardNo", "");
        String dob = sharedPreferences.getString("CustomerID", "");
        String email = sharedPreferences.getString("email", "");
        String firstName = sharedPreferences.getString("firstName", "");
        String lastName = sharedPreferences.getString("lastName", "");
        String mobile = sharedPreferences.getString("mobile", "");

        Log.e("mobile", mobile + " ");
        Log.e("email", email + "");


        if (mobile != null && email != null) {
            Intent ob = new Intent(ViewPagerActivity.this, NavigationDrawerActivity.class);
            startActivity(ob);
        }

    }

}
