package com.crown.easysavecustomerapp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.adapter.ExpandablelistNavigationAdapter;
import com.crown.easysavecustomerapp.fragments.HomeFragment;
import com.crown.easysavecustomerapp.fragments.NotificationFragment;
import com.crown.easysavecustomerapp.fragments.ProfileFragment;
import com.crown.easysavecustomerapp.fragments.WalletFragment;
import com.crown.easysavecustomerapp.utils.SessionManager;
import com.crown.easysavecustomerapp.utils.Share;
import com.crown.easysavecustomerapp.utils.UpdateName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NavigationDrawerActivity extends AppCompatActivity implements View.OnClickListener, UpdateName {
    public static String cardNo;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    public static ImageView ivHome, ivWallet, ivProfile, ivNotification;
    public static TextView tvHome, tvWallet, tvProfile, tvNotification;
    HashMap<String, List<String>> listDataChild;
    ProgressDialog pd;
    LinearLayout linearLayoutHome, linearLayoutWallet, linearLayoutProfile, linearLayoutNotification;
    public static LinearLayout linearLayoutBottomFragment;
    ImageView img;
    TextView textView;
    public static DrawerLayout drawer;

    ImageView profile_img, logo_img;
    TextView navHeaderName;
    TextView tvgo;

    NavigationDrawerActivity navigationDrawerActivity;
    SessionManager sessionManager;

//    Button btn_myProfile, btn_logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        sessionManager = new SessionManager(this);

        navigationDrawerActivity = this;
        pd = new ProgressDialog(NavigationDrawerActivity.this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);

        setDrawer();
        findWidgets();
        prepareListData();
        listAdapter = new ExpandablelistNavigationAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                String selected = (String) listAdapter.getGroup(groupPosition);

                Intent intent;
                switch (selected) {
                    case "Buy a Ticket":
                        intent = new Intent(NavigationDrawerActivity.this, TicketsActivity.class);
                        startActivity(intent);
                        break;

                    case "Our Partners":
                        intent = new Intent(NavigationDrawerActivity.this, OurPartnersActivity.class);
                        startActivity(intent);
                        break;

                    /*case "About EasySave":
                        intent = new Intent(NavigationDrawerActivity.this, AboutUsActivity.class);
                        startActivity(intent);
                        break;*/

                    case "Contact Us":
                        intent = new Intent(NavigationDrawerActivity.this, ContactUsActivity.class);
                        startActivity(intent);
                        break;

                    case "Share With Friends":
                        Share in = new Share();
                        in.shareLink(NavigationDrawerActivity.this);
                        break;

                  /*  case "Weeklys":
                        Intent browserIntents = new Intent(NavigationDrawerActivity.this, WeeklyActivity.class);
                        startActivity(browserIntents);
                        break;*/
                    case "Like Us-Facebook":
                        Intent browserIntentFb = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/easysaverewards/"));
                        startActivity(browserIntentFb);
                        //Toast.makeText(NavigationDrawerActivity.this,"fb",Toast.LENGTH_SHORT).show();
                        break;
                    case "Follow us on Instagram":
                        Intent browserIntentInsta = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/easysavecard/"));
                        startActivity(browserIntentInsta);
                        // Toast.makeText(NavigationDrawerActivity.this, "insta", Toast.LENGTH_SHORT).show();
                        break;
                    case "Logout":
                        AlertDialog.Builder ab = new AlertDialog.Builder(NavigationDrawerActivity.this);
                        ab.setMessage("Are you want to Logout");
                        ab.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                sessionManager.logoutUser();
                                finish();

                                /*Intent inLogout = new Intent(NavigationDrawerActivity.this, LoginSignUpActivity.class);
                                startActivity(inLogout);*/
                            }
                        });
                        ab.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        });

                        ab.show();
                        break;


                }
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                String selected = (String) listAdapter.getChild(groupPosition, childPosition);
                Intent intent;
                switch (selected) {

                    case "Bills":
                        intent = new Intent(NavigationDrawerActivity.this, BillsActivityHistory.class);
                        startActivity(intent);
                        break;

                    case "Rewards":
                        intent = new Intent(NavigationDrawerActivity.this, RewardHistoryActivity.class);
                        startActivity(intent);
                        break;

                    case "Loading":
                        intent = new Intent(NavigationDrawerActivity.this, LoadingHistoryActivity.class);
                        startActivity(intent);
                        break;

                    case "Send/Receive":
                        intent = new Intent(NavigationDrawerActivity.this, SendReceiveHistoryActivity.class);
                        startActivity(intent);
                        break;

                    case "Tickets":
                        intent = new Intent(NavigationDrawerActivity.this, TicketsActivity.class);
                        startActivity(intent);
                        break;

                }

                return false;
            }
        });

        String firstName = sessionManager.getKeyFirstName();
        String lastName = sessionManager.getKeyLastName();

        navHeaderName.setText(firstName + " " + lastName);


        Fragment fragment = new HomeFragment();
        FragmentTransaction quote_fm = getSupportFragmentManager().beginTransaction();
        quote_fm.replace(R.id.content_frame, fragment);
        quote_fm.commit();

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void findWidgets() {

        expListView = (ExpandableListView) findViewById(R.id.list);
        profile_img = (ImageView) findViewById(R.id.profile_img);
        logo_img = (ImageView) findViewById(R.id.img_logo);
        navHeaderName = (TextView) findViewById(R.id.tv_name_header);
        tvgo = (TextView) findViewById(R.id.tv_go);

        linearLayoutHome = (LinearLayout) findViewById(R.id.linearLayout_home);
        linearLayoutWallet = (LinearLayout) findViewById(R.id.linearLayout_wallet);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayout_profile);
        linearLayoutNotification = (LinearLayout) findViewById(R.id.linearLayout_notification);
        linearLayoutBottomFragment = (LinearLayout) findViewById(R.id.linearLayout_bottomNav);

        ivHome = (ImageView) findViewById(R.id.img_bottom_home);
        ivWallet = (ImageView) findViewById(R.id.img_bottom_wallet);
        ivProfile = (ImageView) findViewById(R.id.img_bottom_profile);
        ivNotification = (ImageView) findViewById(R.id.img_bottom_notification);

        tvHome = (TextView) findViewById(R.id.tv_bottom_home);
        tvWallet = (TextView) findViewById(R.id.tv_bottom_wallet);
        tvProfile = (TextView) findViewById(R.id.tv_bottom_profile);
        tvNotification = (TextView) findViewById(R.id.tv_bottom_notification);

        linearLayoutWallet.setOnClickListener(this);
        linearLayoutProfile.setOnClickListener(this);
        linearLayoutHome.setOnClickListener(this);
        linearLayoutNotification.setOnClickListener(this);
        tvgo.setOnClickListener(this);


//                    ================SharedPreferences=====================


    }


    private void prepareListData() {

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Your History");
       // listDataHeader.add("Buy a Ticket");
        listDataHeader.add("Our Partners");
        //listDataHeader.add("About EasySave");
        listDataHeader.add("Contact Us");
        listDataHeader.add("Share With Friends");
        //listDataHeader.add("Weeklys");
        listDataHeader.add("Like Us-Facebook");
        listDataHeader.add("Follow us on Instagram");
        listDataHeader.add("Logout");


        // Adding child data
        List<String> yourHistory = new ArrayList<String>();
       // yourHistory.add("Bills");
        yourHistory.add("Rewards");
        yourHistory.add("Loading");
        yourHistory.add("Send/Receive");
       // yourHistory.add("Tickets");

        listDataChild.put(listDataHeader.get(0), yourHistory);

    }

    private void setDrawer() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

  /*  @Override
    public void onBackPressed() {
        super.onBackPressed();
        // Toast.makeText(HomeActivity.this, "App close", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }*/


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_go:

                drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                //Toast.makeText(NavigationDrawerActivity.this,"Hello",Toast.LENGTH_SHORT).show();
                /*NavigationDrawerActivity.linearLayoutBottomFragment.setVisibility(View.VISIBLE);

                Intent i = new Intent(NavigationDrawerActivity.this, MyProfileActivity.class);
                startActivity(i);*/

                Fragment fragmentp = new ProfileFragment(this, getApplicationContext());
                FragmentTransaction fragmentTransactionp = getSupportFragmentManager().beginTransaction();
                fragmentTransactionp.replace(R.id.content_frame, fragmentp);
                fragmentTransactionp.commit();
                break;
            case R.id.linearLayout_profile:
                Fragment fragment = new ProfileFragment(this, getApplicationContext());
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.commit();
                break;
            case R.id.linearLayout_home:
                Fragment homeFragment = new HomeFragment();
                FragmentTransaction fragmentTransactionHome = getSupportFragmentManager().beginTransaction();
                fragmentTransactionHome.replace(R.id.content_frame, homeFragment);
                fragmentTransactionHome.commit();
                break;
            case R.id.linearLayout_wallet:
                Fragment walletFragment = new WalletFragment();
                FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
                beginTransaction.replace(R.id.content_frame, walletFragment);
                beginTransaction.commit();
                break;
            case R.id.linearLayout_notification:
                Fragment notificationFragment = new NotificationFragment();
                FragmentTransaction beginTransactionNoti = getSupportFragmentManager().beginTransaction();
                beginTransactionNoti.replace(R.id.content_frame, notificationFragment);
                beginTransactionNoti.commit();
                break;

        }
    }


    @Override
    public void setName(String name) {
        String firstName = sessionManager.getKeyFirstName();
        String lastName = sessionManager.getKeyLastName();

        navHeaderName.setText(name);
    }
}
