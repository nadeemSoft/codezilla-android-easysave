package com.crown.easysavecustomerapp.activities;

import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.fragments.LoginScreenFragment;
import com.crown.easysavecustomerapp.fragments.SignUpScreenFragment;
import com.crown.easysavecustomerapp.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by codezilla-10 on 7/3/18.
 */

public class LoginSignUpActivity extends AppCompatActivity {
    String androidId = null;
    TabLayout tabLayout;
    ViewPager viewPager;
    SessionManager sessionManager = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_login_signup_layout);
        sessionManager = new SessionManager(this);
        getSupportActionBar().hide();


        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    protected void onResume() {
        super.onResume();
        androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        sessionManager.saveDeviceId(androidId);
    }

    private void setupViewPager(ViewPager viewPager) {

        LoginSignUpActivity.ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginScreenFragment(), "Login");
        adapter.addFragment(new SignUpScreenFragment(), "Sign Up");
        viewPager.setAdapter(adapter);
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
