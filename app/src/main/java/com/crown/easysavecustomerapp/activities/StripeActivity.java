package com.crown.easysavecustomerapp.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crown.easysavecustomerapp.R;
import com.crown.easysavecustomerapp.helper.AppConstant;
import com.crown.easysavecustomerapp.utils.SessionManager;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputListener;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class StripeActivity extends AppCompatActivity implements View.OnClickListener {
    private String publis_Key_Test = "pk_test_OMbK8nJJPr4vAJLPoMqpenRw";

    private String publis_Key_Live = "pk_live_H7MwVVdBSuwnuBXiN5Xk1oev";

    private Activity activity = StripeActivity.this;

    int amount;
    //CardInputWidget mCardInputWidget = null;
    EditText mCardInputWidget, etCardHolderName, etCardCvv, etMonth, etYear, etMobileNumberNew;

    ProgressDialog pd;
    SessionManager sessionManager;


    Button btnSendMoney, btnPayBill, btnLoadMoneyNew;
    TextView textViewAmount;
    LinearLayout rlPayStripe;
    LinearLayout llStripeConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe2);
        Intent intent = getIntent();
        sessionManager = new SessionManager(StripeActivity.this);
        initWidgets();
        amount = intent.getIntExtra("amount", 0);
        Log.e("amountSTripe",amount+"");

        etMonth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("TestLength", s.length() + "");
                if (s.length() == 2) {
                    etYear.setEnabled(true);
                    etYear.requestFocus();
                    etYear.findFocus();
                    etYear.setFocusable(true);


                }

            }
        });


        etYear.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 2) {
                    etCardCvv.setEnabled(true);
                    etCardCvv.requestFocus();
                    etCardCvv.findFocus();
                    etCardCvv.setFocusable(true);


                } else if (s.length() == 0) {

                    etMonth.setEnabled(true);
                    etMonth.requestFocus();
                    etMonth.findFocus();
                    etMonth.setFocusable(true);

                    etYear.setEnabled(false);
                    etCardCvv.setEnabled(false);

                }

            }
        });

        etCardCvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    etYear.setEnabled(true);
                    etYear.requestFocus();
                    etYear.findFocus();
                    etYear.setFocusable(true);
                    etCardCvv.setEnabled(false);


                }
            }
        });


    }

    private void initWidgets() {
        mCardInputWidget = (EditText) findViewById(R.id.card_input_widget);

        etMonth = findViewById(R.id.et_month);
        etYear = findViewById(R.id.et_year);
        etCardCvv = findViewById(R.id.et_pay_cvv);
        etCardHolderName = findViewById(R.id.card_holder_name);
        etMobileNumberNew = findViewById(R.id.et_number_new);

        pd = new ProgressDialog(this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        textViewAmount = (TextView) findViewById(R.id.paymentAmount);
        btnPayBill = (Button) findViewById(R.id.btn_pay_bill);
        btnSendMoney = (Button) findViewById(R.id.btn_send_money_confirmation);
        btnSendMoney.setEnabled(false);
        btnPayBill.setEnabled(false);

        rlPayStripe = findViewById(R.id.ll_pay_with_stripe);
        llStripeConfirm = findViewById(R.id.ll_pay_confirm);
        btnLoadMoneyNew = findViewById(R.id.buttonPay_load_new);

        btnSendMoney.setOnClickListener(this);
        btnPayBill.setOnClickListener(this);
        btnLoadMoneyNew.setOnClickListener(this);

    }

    private void callToToken(String cardNo, int expMon, int ExpYea, String cvc) {
        pd.show();

        //Card card = new Card("4242-4242-4242-4242", 12, 2019, "123");
        Card card = new Card(cardNo, expMon, ExpYea, cvc);


        if (card == null) {
            pd.dismiss();

            Toast.makeText(StripeActivity.this, "Invalid card data", Toast.LENGTH_SHORT).show();
        } else {
            Stripe stripe = new Stripe(StripeActivity.this, publis_Key_Test);
            stripe.createToken(card,
                    new TokenCallback() {
                        public void onSuccess(Token token) {
                            String tokenId = token.getId();
                            Log.e("StripeTokenId", token.getId() + "");
                            loadPoint(tokenId);


                        }

                        public void onError(Exception error) {
                            pd.dismiss();
                            Log.e("Error", String.valueOf(error));
                            Toast.makeText(StripeActivity.this, String.valueOf(error.getMessage()), Toast.LENGTH_LONG).show();

                        }
                    }
            );


        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_pay_bill:
                Intent intentPay = new Intent(this, PaySeeAllActivity.class);
                startActivity(intentPay);
                finish();
                break;

            case R.id.btn_send_money_confirmation:

                Intent intentSend = new Intent(this, SendMoneyActivity.class);
                startActivity(intentSend);
                finish();
                break;

            case R.id.buttonPay_load_new:

                if (PaymentCardValidation()) {
                    String cardNumber = mCardInputWidget.getText().toString();
                    String cardHolderName = etCardHolderName.getText().toString();
                    int cardExpMonth = Integer.parseInt(etMonth.getText().toString());
                    int cardExpYear = Integer.parseInt(etYear.getText().toString());
                    String cardExpCvvNo = etCardCvv.getText().toString();

                    Log.e("card_num", cardNumber + "");
                    Log.e("exp_month", cardExpMonth + "");
                    Log.e("exp_year", cardExpYear + "");
                    Log.e("cvv", cardExpCvvNo + "");

                    callToToken(cardNumber, cardExpMonth, cardExpYear, cardExpCvvNo);

                }
                break;
        }
    }


    private boolean PaymentCardValidation() {

        if (etCardHolderName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter Card Holder Name", Toast.LENGTH_SHORT).show();
            return false;

        } else if (mCardInputWidget.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter Card Number", Toast.LENGTH_SHORT).show();
            return false;


        } else if (mCardInputWidget.getText().toString().length() != 16) {
            Toast.makeText(activity, "Please Enter 16 Digit Card Number", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etMonth.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter Month", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etMonth.getText().toString().length() != 2) {
            Toast.makeText(activity, "Please Enter Valid Month", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etYear.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter Year", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etYear.getText().toString().length() != 2) {
            Toast.makeText(activity, "Please Enter Valid  Year", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etCardCvv.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter Cvv", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etCardCvv.getText().toString().length() != 3) {
            Toast.makeText(activity, "Please Enter 3 digit Cvv", Toast.LENGTH_SHORT).show();
            return false;

        } /*else if (etMobileNumberNew.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etMobileNumberNew.getText().toString().length() != 10) {
            Toast.makeText(activity, "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        return true;
    }


    private void loadPoint(String token) {
        JSONObject params = new JSONObject();
        try {

            params.put("key", "EASYSAVECUSTOMERAPIV1-10001");
            params.put("cardNo", sessionManager.getKeyCardNumber());
            params.put("loadAmount", sessionManager.getKeyLoadAmount());
            params.put("fee", sessionManager.getKeyFee());
            params.put("amountCollected", sessionManager.getKeyTotalAmount());
            params.put("stripeToken", token);
            params.put("stripePaymentStatus", "success");
            params.put("stripeReasonForFailure", "");


            // OLD PARAMETERS
           /* params.put("key", AppConstant.KEY);
            params.put("cardNo", sessionManager.getKeyCardNumber());
            params.put("loadAmount", sessionManager.getKeyLoadAmount());
            params.put("fee", sessionManager.getKeyFee());
            params.put("amountCollected", sessionManager.getKeyTotalAmount());
            params.put("State", "");
            params.put("paypalTransactionId", "");
            params.put("reasonOfFailure", "");
            params.put("paymentOption", "stripe");
            params.put("stripeToken", token);
            params.put("stripePaymentStatus", "success");
            params.put("stripeReasonForFailure", "");*/


            Log.e("params", params + "");


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(StripeActivity.this, "JSONException " + e, Toast.LENGTH_SHORT).show();

        }

        RequestQueue requestQueue = Volley.newRequestQueue(StripeActivity.this);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, AppConstant.LOAD_POINT, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    Log.e("loadResponseInStrip", response + "");

                    try {
                        JSONObject json = new JSONObject(String.valueOf(response));

                        JSONObject jsonObjectHeader = json.getJSONObject("header");
                        String errorCode = jsonObjectHeader.optString("ErrorCode");
                        String errorMessage = jsonObjectHeader.optString("ErrorMessage");
                        //Toast.makeText(ConfirmationActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                        if (errorCode.equals("0")) {
                            rlPayStripe.setVisibility(View.GONE);
                            llStripeConfirm.setVisibility(View.VISIBLE);
                            JSONObject jsonObject = json.getJSONObject("response");
                            JSONObject objMyPoints = jsonObject.getJSONObject("myPoints");
                            String totalCash = objMyPoints.getString("totalCash");
                            String totalPoints = objMyPoints.getString("totalPoints");
                            sessionManager.savePoints(totalCash, totalPoints);
                            textViewAmount.setText("$" + sessionManager.getActualAmount());
                            pd.dismiss();
                            btnSendMoney.setEnabled(true);
                            btnPayBill.setEnabled(true);

                        } else {

                            Toast.makeText(StripeActivity.this, "" + errorMessage, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(StripeActivity.this, NavigationDrawerActivity.class);
                            startActivity(intent);
                            pd.dismiss();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        pd.dismiss();
                        Toast.makeText(StripeActivity.this, "JSONException" + e, Toast.LENGTH_LONG).show();

                    }

                } else {
                    pd.dismiss();
                    Toast.makeText(StripeActivity.this, "Response" + response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                pd.dismiss();
                Toast.makeText(StripeActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        })


        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");

                return headers;
            }

        };


        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(jsonObjectRequest);
    }


}
